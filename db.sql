-- phpMyAdmin SQL Dump
-- version 
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 26 2021 г., 14:25
-- Версия сервера: 5.7.33-36-log
-- Версия PHP: 7.4.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `host1384129_wp`
--

-- --------------------------------------------------------

--
-- Структура таблицы `wp_actionscheduler_actions`
--

CREATE TABLE `wp_actionscheduler_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `hook` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `scheduled_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `scheduled_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `args` varchar(191) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `schedule` longtext COLLATE utf8mb4_unicode_520_ci,
  `group_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `attempts` int(11) NOT NULL DEFAULT '0',
  `last_attempt_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `last_attempt_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `claim_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `extended_args` varchar(8000) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_actionscheduler_actions`
--

INSERT INTO `wp_actionscheduler_actions` (`action_id`, `hook`, `status`, `scheduled_date_gmt`, `scheduled_date_local`, `args`, `schedule`, `group_id`, `attempts`, `last_attempt_gmt`, `last_attempt_local`, `claim_id`, `extended_args`) VALUES
(50, 'action_scheduler/migration_hook', 'complete', '2021-04-22 10:28:54', '2021-04-22 10:28:54', '[]', 'O:30:\"ActionScheduler_SimpleSchedule\":2:{s:22:\"\0*\0scheduled_timestamp\";i:1619087334;s:41:\"\0ActionScheduler_SimpleSchedule\0timestamp\";i:1619087334;}', 1, 1, '2021-04-22 10:28:55', '2021-04-22 16:28:55', 0, NULL),
(51, 'wp_mail_smtp_admin_notifications_update', 'complete', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '[1]', 'O:28:\"ActionScheduler_NullSchedule\":0:{}', 2, 1, '2021-04-22 10:29:54', '2021-04-22 16:29:54', 0, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_actionscheduler_claims`
--

CREATE TABLE `wp_actionscheduler_claims` (
  `claim_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_actionscheduler_groups`
--

CREATE TABLE `wp_actionscheduler_groups` (
  `group_id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_actionscheduler_groups`
--

INSERT INTO `wp_actionscheduler_groups` (`group_id`, `slug`) VALUES
(1, 'action-scheduler-migration'),
(2, 'wp_mail_smtp');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_actionscheduler_logs`
--

CREATE TABLE `wp_actionscheduler_logs` (
  `log_id` bigint(20) UNSIGNED NOT NULL,
  `action_id` bigint(20) UNSIGNED NOT NULL,
  `message` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `log_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `log_date_local` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_actionscheduler_logs`
--

INSERT INTO `wp_actionscheduler_logs` (`log_id`, `action_id`, `message`, `log_date_gmt`, `log_date_local`) VALUES
(1, 50, 'action created', '2021-04-22 10:27:54', '2021-04-22 10:27:54'),
(2, 50, 'action started via WP Cron', '2021-04-22 10:28:55', '2021-04-22 10:28:55'),
(3, 50, 'action complete via WP Cron', '2021-04-22 10:28:55', '2021-04-22 10:28:55'),
(4, 51, 'action created', '2021-04-22 10:29:49', '2021-04-22 10:29:49'),
(5, 51, 'action started via WP Cron', '2021-04-22 10:29:54', '2021-04-22 10:29:54'),
(6, 51, 'action complete via WP Cron', '2021-04-22 10:29:54', '2021-04-22 10:29:54');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_commentmeta`
--

CREATE TABLE `wp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_comments`
--

CREATE TABLE `wp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'comment',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_links`
--

CREATE TABLE `wp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_options`
--

CREATE TABLE `wp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_options`
--

INSERT INTO `wp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://wt.iilw.ru', 'yes'),
(2, 'home', 'http://wt.iilw.ru', 'yes'),
(3, 'blogname', 'Мы вместе', 'yes'),
(4, 'blogdescription', '', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'project_bf@mail.ru', 'yes'),
(7, 'start_of_week', '1', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '10', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', '', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '', 'yes'),
(22, 'posts_per_page', '10', 'yes'),
(23, 'date_format', 'd.m.Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'd.m.Y H:i', 'yes'),
(26, 'comment_moderation', '', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:117:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:17:\"^wp-sitemap\\.xml$\";s:23:\"index.php?sitemap=index\";s:17:\"^wp-sitemap\\.xsl$\";s:36:\"index.php?sitemap-stylesheet=sitemap\";s:23:\"^wp-sitemap-index\\.xsl$\";s:34:\"index.php?sitemap-stylesheet=index\";s:48:\"^wp-sitemap-([a-z]+?)-([a-z\\d_-]+?)-(\\d+?)\\.xml$\";s:75:\"index.php?sitemap=$matches[1]&sitemap-subtype=$matches[2]&paged=$matches[3]\";s:34:\"^wp-sitemap-([a-z]+?)-(\\d+?)\\.xml$\";s:47:\"index.php?sitemap=$matches[1]&paged=$matches[2]\";s:15:\"distributors/?$\";s:37:\"index.php?post_type=post_distributors\";s:45:\"distributors/feed/(feed|rdf|rss|rss2|atom)/?$\";s:54:\"index.php?post_type=post_distributors&feed=$matches[1]\";s:40:\"distributors/(feed|rdf|rss|rss2|atom)/?$\";s:54:\"index.php?post_type=post_distributors&feed=$matches[1]\";s:32:\"distributors/page/([0-9]{1,})/?$\";s:55:\"index.php?post_type=post_distributors&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:40:\"distributors/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:50:\"distributors/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:70:\"distributors/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"distributors/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:65:\"distributors/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:46:\"distributors/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:29:\"distributors/([^/]+)/embed/?$\";s:50:\"index.php?post_distributors=$matches[1]&embed=true\";s:33:\"distributors/([^/]+)/trackback/?$\";s:44:\"index.php?post_distributors=$matches[1]&tb=1\";s:53:\"distributors/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?post_distributors=$matches[1]&feed=$matches[2]\";s:48:\"distributors/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?post_distributors=$matches[1]&feed=$matches[2]\";s:41:\"distributors/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?post_distributors=$matches[1]&paged=$matches[2]\";s:48:\"distributors/([^/]+)/comment-page-([0-9]{1,})/?$\";s:57:\"index.php?post_distributors=$matches[1]&cpage=$matches[2]\";s:37:\"distributors/([^/]+)(?:/([0-9]+))?/?$\";s:56:\"index.php?post_distributors=$matches[1]&page=$matches[2]\";s:29:\"distributors/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:39:\"distributors/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:59:\"distributors/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"distributors/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:54:\"distributors/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:35:\"distributors/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:12:\"robots\\.txt$\";s:18:\"index.php?robots=1\";s:13:\"favicon\\.ico$\";s:19:\"index.php?favicon=1\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:38:\"index.php?&page_id=7&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:10:{i:0;s:41:\"advanced-custom-fields-pro-master/acf.php\";i:1;s:36:\"contact-form-7/wp-contact-form-7.php\";i:2;s:22:\"cyr2lat/cyr-to-lat.php\";i:3;s:29:\"duplicate-pp/duplicate-pp.php\";i:4;s:21:\"flamingo/flamingo.php\";i:5;s:23:\"loco-translate/loco.php\";i:6;s:47:\"regenerate-thumbnails/regenerate-thumbnails.php\";i:7;s:27:\"svg-support/svg-support.php\";i:8;s:29:\"wp-mail-smtp/wp_mail_smtp.php\";i:9;s:23:\"wp-smushit/wp-smush.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '6', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'wp-bootstrap-starter', 'yes'),
(41, 'stylesheet', 'wt-theme', 'yes'),
(42, 'comment_registration', '', 'yes'),
(43, 'html_type', 'text/html', 'yes'),
(44, 'use_trackback', '0', 'yes'),
(45, 'default_role', 'subscriber', 'yes'),
(46, 'db_version', '49752', 'yes'),
(47, 'uploads_use_yearmonth_folders', '1', 'yes'),
(48, 'upload_path', '', 'yes'),
(49, 'blog_public', '0', 'yes'),
(50, 'default_link_category', '2', 'yes'),
(51, 'show_on_front', 'page', 'yes'),
(52, 'tag_base', '', 'yes'),
(53, 'show_avatars', '1', 'yes'),
(54, 'avatar_rating', 'G', 'yes'),
(55, 'upload_url_path', '', 'yes'),
(56, 'thumbnail_size_w', '150', 'yes'),
(57, 'thumbnail_size_h', '150', 'yes'),
(58, 'thumbnail_crop', '1', 'yes'),
(59, 'medium_size_w', '300', 'yes'),
(60, 'medium_size_h', '300', 'yes'),
(61, 'avatar_default', 'mystery', 'yes'),
(62, 'large_size_w', '1024', 'yes'),
(63, 'large_size_h', '1024', 'yes'),
(64, 'image_default_link_type', 'none', 'yes'),
(65, 'image_default_size', '', 'yes'),
(66, 'image_default_align', '', 'yes'),
(67, 'close_comments_for_old_posts', '', 'yes'),
(68, 'close_comments_days_old', '14', 'yes'),
(69, 'thread_comments', '1', 'yes'),
(70, 'thread_comments_depth', '5', 'yes'),
(71, 'page_comments', '', 'yes'),
(72, 'comments_per_page', '50', 'yes'),
(73, 'default_comments_page', 'newest', 'yes'),
(74, 'comment_order', 'asc', 'yes'),
(75, 'sticky_posts', 'a:0:{}', 'yes'),
(76, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(77, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(78, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'uninstall_plugins', 'a:0:{}', 'no'),
(80, 'timezone_string', '', 'yes'),
(81, 'page_for_posts', '0', 'yes'),
(82, 'page_on_front', '7', 'yes'),
(83, 'default_post_format', '0', 'yes'),
(84, 'link_manager_enabled', '0', 'yes'),
(85, 'finished_splitting_shared_terms', '1', 'yes'),
(86, 'site_icon', '107', 'yes'),
(87, 'medium_large_size_w', '768', 'yes'),
(88, 'medium_large_size_h', '0', 'yes'),
(89, 'wp_page_for_privacy_policy', '3', 'yes'),
(90, 'show_comments_cookies_opt_in', '1', 'yes'),
(91, 'admin_email_lifespan', '1634636331', 'yes'),
(92, 'disallowed_keys', '', 'no'),
(93, 'comment_previously_approved', '1', 'yes'),
(94, 'auto_plugin_theme_update_emails', 'a:0:{}', 'no'),
(95, 'auto_update_core_dev', 'enabled', 'yes'),
(96, 'auto_update_core_minor', 'enabled', 'yes'),
(97, 'auto_update_core_major', 'enabled', 'yes'),
(98, 'initial_db_version', '49752', 'yes'),
(99, 'wp_user_roles', 'a:6:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:10:\"loco_admin\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:34:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:10:\"translator\";a:2:{s:4:\"name\";s:10:\"Translator\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:10:\"loco_admin\";b:1;}}}', 'yes'),
(100, 'fresh_site', '0', 'yes'),
(101, 'WPLANG', 'ru_RU', 'yes'),
(102, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(103, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(104, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'sidebars_widgets', 'a:6:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}s:13:\"array_version\";i:3;}', 'yes'),
(108, 'cron', 'a:9:{i:1619431854;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"0d04ed39571b55704c122d726248bbac\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:1:{i:0;s:7:\"WP Cron\";}s:8:\"interval\";i:60;}}}i:1619432998;a:1:{s:23:\"flamingo_daily_cron_job\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1619433532;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1619473132;a:4:{s:18:\"wp_https_detection\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1619516332;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1619516356;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1619516371;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1619775532;a:1:{s:30:\"wp_site_health_scheduled_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"weekly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:604800;}}}s:7:\"version\";i:2;}', 'yes'),
(109, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(113, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(115, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(116, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(117, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(119, 'recovery_keys', 'a:0:{}', 'yes'),
(120, 'https_detection_errors', 'a:1:{s:20:\"https_request_failed\";a:1:{i:0;s:36:\"HTTPS запрос неудачен.\";}}', 'yes'),
(122, 'theme_mods_twentytwentyone', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1619084389;s:4:\"data\";a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:3:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";}s:9:\"sidebar-2\";a:3:{i:0;s:10:\"archives-2\";i:1;s:12:\"categories-2\";i:2;s:6:\"meta-2\";}}}}', 'yes'),
(128, '_site_transient_timeout_browser_430c8bfe8bf1cc772d8a7ec27f27106c', '1619689142', 'no'),
(129, '_site_transient_browser_430c8bfe8bf1cc772d8a7ec27f27106c', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"89.0.4389.128\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(130, '_site_transient_timeout_php_check_97f83d63b8a66f6e8c057d89a83d8845', '1619689143', 'no'),
(131, '_site_transient_php_check_97f83d63b8a66f6e8c057d89a83d8845', 'a:5:{s:19:\"recommended_version\";s:3:\"7.4\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:0;s:9:\"is_secure\";b:0;s:13:\"is_acceptable\";b:0;}', 'no'),
(132, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.7.1.zip\";s:6:\"locale\";s:5:\"ru_RU\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/ru_RU/wordpress-5.7.1.zip\";s:10:\"no_content\";s:0:\"\";s:11:\"new_bundled\";s:0:\"\";s:7:\"partial\";s:0:\"\";s:8:\"rollback\";s:0:\"\";}s:7:\"current\";s:5:\"5.7.1\";s:7:\"version\";s:5:\"5.7.1\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.6\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1619431813;s:15:\"version_checked\";s:5:\"5.7.1\";s:12:\"translations\";a:0:{}}', 'no'),
(133, 'can_compress_scripts', '0', 'no'),
(134, '_site_transient_update_themes', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1619431814;s:7:\"checked\";a:2:{s:20:\"wp-bootstrap-starter\";s:5:\"3.3.6\";s:8:\"wt-theme\";s:5:\"1.0.0\";}s:8:\"response\";a:0:{}s:9:\"no_update\";a:1:{s:20:\"wp-bootstrap-starter\";a:6:{s:5:\"theme\";s:20:\"wp-bootstrap-starter\";s:11:\"new_version\";s:5:\"3.3.6\";s:3:\"url\";s:50:\"https://wordpress.org/themes/wp-bootstrap-starter/\";s:7:\"package\";s:68:\"https://downloads.wordpress.org/theme/wp-bootstrap-starter.3.3.6.zip\";s:8:\"requires\";s:3:\"4.7\";s:12:\"requires_php\";s:5:\"5.2.4\";}}s:12:\"translations\";a:0:{}}', 'no'),
(150, 'current_theme', 'Wt-theme', 'yes'),
(151, 'theme_mods_hookan-theme', 'a:4:{i:0;b:0;s:18:\"nav_menu_locations\";a:0:{}s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1619084937;s:4:\"data\";a:5:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:0:{}s:8:\"footer-1\";a:0:{}s:8:\"footer-2\";a:0:{}s:8:\"footer-3\";a:0:{}}}}', 'yes'),
(152, 'theme_switched', '', 'yes'),
(153, 'triggered_welcomet', '1', 'yes'),
(154, 'recently_activated', 'a:0:{}', 'yes'),
(155, 'wpcf7', 'a:2:{s:7:\"version\";s:3:\"5.4\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";i:1619084411;s:7:\"version\";s:3:\"5.4\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(156, 'acf_version', '5.9.1', 'yes'),
(159, 'finished_updating_comment_type', '1', 'yes'),
(162, 'new_admin_email', 'project_bf@mail.ru', 'yes'),
(184, 'theme_mods_wt-theme', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:7:\"primary\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(187, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(190, 'wp-smush-settings', 'a:22:{s:4:\"auto\";b:1;s:5:\"lossy\";b:0;s:10:\"strip_exif\";b:1;s:6:\"resize\";b:0;s:9:\"detection\";b:0;s:8:\"original\";b:0;s:6:\"backup\";b:0;s:10:\"png_to_jpg\";b:0;s:7:\"nextgen\";b:0;s:2:\"s3\";b:0;s:9:\"gutenberg\";b:0;s:10:\"js_builder\";b:0;s:3:\"cdn\";b:0;s:11:\"auto_resize\";b:0;s:4:\"webp\";b:1;s:5:\"usage\";b:0;s:17:\"accessible_colors\";b:0;s:9:\"keep_data\";b:1;s:9:\"lazy_load\";b:0;s:17:\"background_images\";b:1;s:16:\"rest_api_support\";b:0;s:8:\"webp_mod\";b:0;}', 'yes'),
(191, 'wp-smush-install-type', 'existing', 'no'),
(192, 'wp-smush-version', '3.8.4', 'no'),
(195, 'bodhi_svgs_plugin_version', '2.3.18', 'yes'),
(196, 'wp_mail_smtp_initial_version', '2.7.0', 'no'),
(197, 'wp_mail_smtp_version', '2.7.0', 'no'),
(198, 'wp_mail_smtp', 'a:2:{s:4:\"mail\";a:6:{s:10:\"from_email\";s:18:\"project_bf@mail.ru\";s:9:\"from_name\";s:17:\"Мы вместе\";s:6:\"mailer\";s:4:\"mail\";s:11:\"return_path\";b:0;s:16:\"from_email_force\";b:1;s:15:\"from_name_force\";b:0;}s:4:\"smtp\";a:2:{s:7:\"autotls\";b:1;s:4:\"auth\";b:1;}}', 'no'),
(199, 'wp_mail_smtp_activated_time', '1619087273', 'no'),
(200, 'wp_mail_smtp_activated', 'a:1:{s:4:\"lite\";i:1619087273;}', 'yes'),
(203, 'action_scheduler_hybrid_store_demarkation', '49', 'yes'),
(204, 'schema-ActionScheduler_StoreSchema', '3.0.1619087274', 'yes'),
(205, 'schema-ActionScheduler_LoggerSchema', '2.0.1619087274', 'yes'),
(206, 'wp_mail_smtp_migration_version', '3', 'yes'),
(209, 'wdev-frash', 'a:3:{s:7:\"plugins\";a:1:{s:23:\"wp-smushit/wp-smush.php\";i:1619087274;}s:5:\"queue\";a:2:{s:32:\"7de3619981caadc55f30a002bfb299f6\";a:4:{s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:4:\"type\";s:5:\"email\";s:7:\"show_at\";i:1619087274;s:6:\"sticky\";b:1;}s:32:\"fc50097023d0d34c5a66f6cddcf77694\";a:3:{s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:4:\"type\";s:4:\"rate\";s:7:\"show_at\";i:1619692074;}}s:4:\"done\";a:0:{}}', 'no'),
(210, 'wpmudev_recommended_plugins_registered', 'a:1:{s:23:\"wp-smushit/wp-smush.php\";a:1:{s:13:\"registered_at\";i:1619087274;}}', 'no'),
(211, 'wp_mail_smtp_review_notice', 'a:2:{s:4:\"time\";i:1619087274;s:9:\"dismissed\";b:0;}', 'yes'),
(212, 'action_scheduler_lock_async-request-runner', '1619179903', 'yes'),
(214, 'options_logo', '49', 'no'),
(215, '_options_logo', 'field_6081487bce81c', 'no'),
(216, 'options_copyright', 'Фонд “Мы вместе” © 2021', 'no'),
(217, '_options_copyright', 'field_6081489bce81d', 'no'),
(218, 'options_policy', '3', 'no'),
(219, '_options_policy', 'field_60814928b086c', 'no'),
(220, 'options_social_links', '2', 'no'),
(221, '_options_social_links', 'field_608148bece81f', 'no'),
(223, 'action_scheduler_migration_status', 'complete', 'yes'),
(229, 'wp_mail_smtp_notifications', 'a:4:{s:6:\"update\";i:1619087394;s:4:\"feed\";a:0:{}s:6:\"events\";a:0:{}s:9:\"dismissed\";a:0:{}}', 'yes'),
(295, 'category_children', 'a:0:{}', 'yes'),
(399, 'options_social_links_0_link', 'https://www.youtube.com/', 'no'),
(400, '_options_social_links_0_link', 'field_608148d1ce820', 'no'),
(401, 'options_social_links_0_icon', '94', 'no'),
(402, '_options_social_links_0_icon', 'field_608168d9cb5ef', 'no'),
(403, 'options_social_links_1_link', 'http://instagram.com/', 'no'),
(404, '_options_social_links_1_link', 'field_608148d1ce820', 'no'),
(405, 'options_social_links_1_icon', '95', 'no'),
(406, '_options_social_links_1_icon', 'field_608168d9cb5ef', 'no'),
(546, '_site_transient_timeout_browser_87532f5c229247f5166d216fad1d829f', '1619719587', 'no'),
(547, '_site_transient_browser_87532f5c229247f5166d216fad1d829f', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:12:\"90.0.4430.85\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(688, '_transient_is_multi_author', '0', 'yes'),
(713, '_transient_health-check-site-status-result', '{\"good\":14,\"recommended\":4,\"critical\":2}', 'yes'),
(724, '_transient_timeout_acf_plugin_updates', '1619522381', 'no'),
(725, '_transient_acf_plugin_updates', 'a:4:{s:7:\"plugins\";a:1:{s:41:\"advanced-custom-fields-pro-master/acf.php\";a:8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:41:\"advanced-custom-fields-pro-master/acf.php\";s:11:\"new_version\";s:5:\"5.9.5\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:3:\"5.7\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:10:\"expiration\";i:172800;s:6:\"status\";i:1;s:7:\"checked\";a:1:{s:41:\"advanced-custom-fields-pro-master/acf.php\";s:5:\"5.9.1\";}}', 'no'),
(731, '_site_transient_timeout_theme_roots', '1619433613', 'no'),
(732, '_site_transient_theme_roots', 'a:2:{s:20:\"wp-bootstrap-starter\";s:7:\"/themes\";s:8:\"wt-theme\";s:7:\"/themes\";}', 'no'),
(733, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1619431815;s:7:\"checked\";a:10:{s:41:\"advanced-custom-fields-pro-master/acf.php\";s:5:\"5.9.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:3:\"5.4\";s:22:\"cyr2lat/cyr-to-lat.php\";s:5:\"5.0.4\";s:29:\"duplicate-pp/duplicate-pp.php\";s:5:\"3.3.1\";s:21:\"flamingo/flamingo.php\";s:5:\"2.2.1\";s:23:\"loco-translate/loco.php\";s:5:\"2.5.2\";s:47:\"regenerate-thumbnails/regenerate-thumbnails.php\";s:5:\"3.1.5\";s:23:\"wp-smushit/wp-smush.php\";s:5:\"3.8.4\";s:27:\"svg-support/svg-support.php\";s:6:\"2.3.18\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:5:\"2.7.0\";}s:8:\"response\";a:1:{s:41:\"advanced-custom-fields-pro-master/acf.php\";O:8:\"stdClass\":8:{s:4:\"slug\";s:26:\"advanced-custom-fields-pro\";s:6:\"plugin\";s:41:\"advanced-custom-fields-pro-master/acf.php\";s:11:\"new_version\";s:5:\"5.9.5\";s:3:\"url\";s:36:\"https://www.advancedcustomfields.com\";s:6:\"tested\";s:3:\"5.7\";s:7:\"package\";s:0:\"\";s:5:\"icons\";a:1:{s:7:\"default\";s:63:\"https://ps.w.org/advanced-custom-fields/assets/icon-256x256.png\";}s:7:\"banners\";a:2:{s:3:\"low\";s:77:\"https://ps.w.org/advanced-custom-fields/assets/banner-772x250.jpg?rev=1729102\";s:4:\"high\";s:78:\"https://ps.w.org/advanced-custom-fields/assets/banner-1544x500.jpg?rev=1729099\";}}}s:12:\"translations\";a:0:{}s:9:\"no_update\";a:9:{s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:3:\"5.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/contact-form-7.5.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=2279696\";s:2:\"1x\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";s:3:\"svg\";s:59:\"https://ps.w.org/contact-form-7/assets/icon.svg?rev=2339255\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:22:\"cyr2lat/cyr-to-lat.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:21:\"w.org/plugins/cyr2lat\";s:4:\"slug\";s:7:\"cyr2lat\";s:6:\"plugin\";s:22:\"cyr2lat/cyr-to-lat.php\";s:11:\"new_version\";s:5:\"5.0.4\";s:3:\"url\";s:38:\"https://wordpress.org/plugins/cyr2lat/\";s:7:\"package\";s:56:\"https://downloads.wordpress.org/plugin/cyr2lat.5.0.4.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:60:\"https://ps.w.org/cyr2lat/assets/icon-256x256.jpg?rev=2434252\";s:2:\"1x\";s:52:\"https://ps.w.org/cyr2lat/assets/icon.svg?rev=2498806\";s:3:\"svg\";s:52:\"https://ps.w.org/cyr2lat/assets/icon.svg?rev=2498806\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/cyr2lat/assets/banner-1544x500.png?rev=2434252\";s:2:\"1x\";s:62:\"https://ps.w.org/cyr2lat/assets/banner-772x250.png?rev=2434252\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"duplicate-pp/duplicate-pp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/duplicate-pp\";s:4:\"slug\";s:12:\"duplicate-pp\";s:6:\"plugin\";s:29:\"duplicate-pp/duplicate-pp.php\";s:11:\"new_version\";s:5:\"3.3.1\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/duplicate-pp/\";s:7:\"package\";s:55:\"https://downloads.wordpress.org/plugin/duplicate-pp.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/duplicate-pp/assets/icon-256x256.png?rev=2149342\";s:2:\"1x\";s:65:\"https://ps.w.org/duplicate-pp/assets/icon-256x256.png?rev=2149342\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-pp/assets/banner-772x250.jpg?rev=2150735\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"flamingo/flamingo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/flamingo\";s:4:\"slug\";s:8:\"flamingo\";s:6:\"plugin\";s:21:\"flamingo/flamingo.php\";s:11:\"new_version\";s:5:\"2.2.1\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/flamingo/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/flamingo.2.2.1.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/flamingo/assets/icon-128x128.png?rev=1540977\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:62:\"https://ps.w.org/flamingo/assets/banner-772x250.png?rev=544829\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"loco-translate/loco.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/loco-translate\";s:4:\"slug\";s:14:\"loco-translate\";s:6:\"plugin\";s:23:\"loco-translate/loco.php\";s:11:\"new_version\";s:5:\"2.5.2\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/loco-translate/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/loco-translate.2.5.2.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-256x256.png?rev=1000676\";s:2:\"1x\";s:67:\"https://ps.w.org/loco-translate/assets/icon-128x128.png?rev=1000676\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:68:\"https://ps.w.org/loco-translate/assets/banner-772x250.jpg?rev=745046\";}s:11:\"banners_rtl\";a:0:{}}s:47:\"regenerate-thumbnails/regenerate-thumbnails.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/regenerate-thumbnails\";s:4:\"slug\";s:21:\"regenerate-thumbnails\";s:6:\"plugin\";s:47:\"regenerate-thumbnails/regenerate-thumbnails.php\";s:11:\"new_version\";s:5:\"3.1.5\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/regenerate-thumbnails/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/regenerate-thumbnails.3.1.5.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:74:\"https://ps.w.org/regenerate-thumbnails/assets/icon-128x128.png?rev=1753390\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:77:\"https://ps.w.org/regenerate-thumbnails/assets/banner-1544x500.jpg?rev=1753390\";s:2:\"1x\";s:76:\"https://ps.w.org/regenerate-thumbnails/assets/banner-772x250.jpg?rev=1753390\";}s:11:\"banners_rtl\";a:0:{}}s:23:\"wp-smushit/wp-smush.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:24:\"w.org/plugins/wp-smushit\";s:4:\"slug\";s:10:\"wp-smushit\";s:6:\"plugin\";s:23:\"wp-smushit/wp-smush.php\";s:11:\"new_version\";s:5:\"3.8.4\";s:3:\"url\";s:41:\"https://wordpress.org/plugins/wp-smushit/\";s:7:\"package\";s:59:\"https://downloads.wordpress.org/plugin/wp-smushit.3.8.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-256x256.gif?rev=2263432\";s:2:\"1x\";s:63:\"https://ps.w.org/wp-smushit/assets/icon-128x128.gif?rev=2263431\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/wp-smushit/assets/banner-1544x500.png?rev=1863697\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-smushit/assets/banner-772x250.png?rev=1863697\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"svg-support/svg-support.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/svg-support\";s:4:\"slug\";s:11:\"svg-support\";s:6:\"plugin\";s:27:\"svg-support/svg-support.php\";s:11:\"new_version\";s:6:\"2.3.18\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/svg-support/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/svg-support.2.3.18.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:64:\"https://ps.w.org/svg-support/assets/icon-256x256.png?rev=1417738\";s:2:\"1x\";s:56:\"https://ps.w.org/svg-support/assets/icon.svg?rev=1417738\";s:3:\"svg\";s:56:\"https://ps.w.org/svg-support/assets/icon.svg?rev=1417738\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/svg-support/assets/banner-1544x500.jpg?rev=1215377\";s:2:\"1x\";s:66:\"https://ps.w.org/svg-support/assets/banner-772x250.jpg?rev=1215377\";}s:11:\"banners_rtl\";a:0:{}}s:29:\"wp-mail-smtp/wp_mail_smtp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:26:\"w.org/plugins/wp-mail-smtp\";s:4:\"slug\";s:12:\"wp-mail-smtp\";s:6:\"plugin\";s:29:\"wp-mail-smtp/wp_mail_smtp.php\";s:11:\"new_version\";s:5:\"2.7.0\";s:3:\"url\";s:43:\"https://wordpress.org/plugins/wp-mail-smtp/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wp-mail-smtp.2.7.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-256x256.png?rev=1755440\";s:2:\"1x\";s:65:\"https://ps.w.org/wp-mail-smtp/assets/icon-128x128.png?rev=1755440\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:68:\"https://ps.w.org/wp-mail-smtp/assets/banner-1544x500.png?rev=2468655\";s:2:\"1x\";s:67:\"https://ps.w.org/wp-mail-smtp/assets/banner-772x250.png?rev=2468655\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_postmeta`
--

CREATE TABLE `wp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_postmeta`
--

INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(2, 3, '_wp_page_template', 'default'),
(10, 7, '_edit_lock', '1619121380:1'),
(11, 9, '_form', '<label> Your name\n    [text* your-name] </label>\n\n<label> Your email\n    [email* your-email] </label>\n\n<label> Тема\n    [text* your-subject] </label>\n\n<label> Your message (optional)\n    [textarea your-message] </label>\n\n[submit \"Submit\"]'),
(12, 9, '_mail', 'a:8:{s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:36:\"[_site_title] <wordpress@wt.iilw.ru>\";s:4:\"body\";s:187:\"От: [your-name] <[your-email]>\nТема: [your-subject]\n\nСообщение:\n[your-message]\n\n-- \nЭто сообщение отправлено с сайта [_site_title] ([_site_url])\";s:9:\"recipient\";s:19:\"[_site_admin_email]\";s:18:\"additional_headers\";s:22:\"Reply-To: [your-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(13, 9, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:30:\"[_site_title] \"[your-subject]\"\";s:6:\"sender\";s:36:\"[_site_title] <wordpress@wt.iilw.ru>\";s:4:\"body\";s:128:\"Сообщение:\n[your-message]\n\n-- \nЭто сообщение отправлено с сайта [_site_title] ([_site_url])\";s:9:\"recipient\";s:12:\"[your-email]\";s:18:\"additional_headers\";s:29:\"Reply-To: [_site_admin_email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";i:0;s:13:\"exclude_blank\";i:0;}'),
(14, 9, '_messages', 'a:12:{s:12:\"mail_sent_ok\";s:92:\"Спасибо за Ваше сообщение. Оно успешно отправлено.\";s:12:\"mail_sent_ng\";s:144:\"При отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\";s:16:\"validation_error\";s:180:\"Одно или несколько полей содержат ошибочные данные. Пожалуйста, проверьте их и попробуйте ещё раз.\";s:4:\"spam\";s:144:\"При отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\";s:12:\"accept_terms\";s:132:\"Вы должны принять условия и положения перед отправкой вашего сообщения.\";s:16:\"invalid_required\";s:60:\"Поле обязательно для заполнения.\";s:16:\"invalid_too_long\";s:39:\"Поле слишком длинное.\";s:17:\"invalid_too_short\";s:41:\"Поле слишком короткое.\";s:13:\"upload_failed\";s:90:\"При загрузке файла произошла неизвестная ошибка.\";s:24:\"upload_file_type_invalid\";s:81:\"Вам не разрешено загружать файлы этого типа.\";s:21:\"upload_file_too_large\";s:39:\"Файл слишком большой.\";s:23:\"upload_failed_php_error\";s:67:\"При загрузке файла произошла ошибка.\";}'),
(15, 9, '_additional_settings', NULL),
(16, 9, '_locale', 'ru_RU'),
(17, 7, '_wp_old_slug', '%d0%b3%d0%bb%d0%b0%d0%b2%d0%bd%d0%b0%d1%8f-%d1%81%d1%82%d1%80%d0%b0%d0%bd%d0%b8%d1%86%d0%b0'),
(18, 10, '_edit_last', '1'),
(19, 10, '_edit_lock', '1619098117:1'),
(20, 18, '_edit_last', '1'),
(21, 18, '_edit_lock', '1619102387:1'),
(22, 7, '_wp_page_template', 'page-templates/front-page.php'),
(23, 7, '_edit_last', '1'),
(24, 7, 'slider', '2'),
(25, 7, '_slider', 'field_60814a6e8e1f2'),
(26, 7, 'about_us_title', 'Бирюков\r\nНиколай Сергеевич'),
(27, 7, '_about_us_title', 'field_60814b3c8e1f9'),
(28, 7, 'about_us_desc', '<h5>«МЫ ВМЕСТЕ» ОТ СЛОВ К ДЕЛУ</h5>\r\nОмская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать. Сейчас самое время применить другой подход к людям, считает учредитель Благотворительного Фонда социальных инициатив «Мы вместе», директор ООО «ПЭТСИБ» Николай Бирюков.'),
(29, 7, '_about_us_desc', 'field_60814b538e1fa'),
(30, 7, 'about_us_img', '52'),
(31, 7, '_about_us_img', 'field_60814b6e8e1fb'),
(32, 7, 'programms_title', 'Программы'),
(33, 7, '_programms_title', 'field_60814b9a8e1fd'),
(34, 7, 'programms_items', 'a:3:{i:0;s:2:\"57\";i:1;s:2:\"59\";i:2;s:2:\"61\";}'),
(35, 7, '_programms_items', 'field_60814bb08e1fe'),
(36, 7, 'news_title', 'Пресс-центр'),
(37, 7, '_news_title', 'field_60814bf98e200'),
(38, 8, 'slider', ''),
(39, 8, '_slider', 'field_60814a6e8e1f2'),
(40, 8, 'about_us_title', ''),
(41, 8, '_about_us_title', 'field_60814b3c8e1f9'),
(42, 8, 'about_us_desc', ''),
(43, 8, '_about_us_desc', 'field_60814b538e1fa'),
(44, 8, 'about_us_img', ''),
(45, 8, '_about_us_img', 'field_60814b6e8e1fb'),
(46, 8, 'programms_title', ''),
(47, 8, '_programms_title', 'field_60814b9a8e1fd'),
(48, 8, 'programms_items', ''),
(49, 8, '_programms_items', 'field_60814bb08e1fe'),
(50, 8, 'news_title', ''),
(51, 8, '_news_title', 'field_60814bf98e200'),
(61, 36, '_edit_lock', '1619114980:1'),
(62, 38, '_edit_lock', '1619121376:1'),
(63, 40, '_edit_lock', '1619114987:1'),
(64, 42, '_edit_lock', '1619114971:1'),
(65, 44, '_menu_item_type', 'post_type'),
(66, 44, '_menu_item_menu_item_parent', '0'),
(67, 44, '_menu_item_object_id', '42'),
(68, 44, '_menu_item_object', 'page'),
(69, 44, '_menu_item_target', ''),
(70, 44, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(71, 44, '_menu_item_xfn', ''),
(72, 44, '_menu_item_url', ''),
(74, 45, '_menu_item_type', 'post_type'),
(75, 45, '_menu_item_menu_item_parent', '0'),
(76, 45, '_menu_item_object_id', '40'),
(77, 45, '_menu_item_object', 'page'),
(78, 45, '_menu_item_target', ''),
(79, 45, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(80, 45, '_menu_item_xfn', ''),
(81, 45, '_menu_item_url', ''),
(83, 46, '_menu_item_type', 'post_type'),
(84, 46, '_menu_item_menu_item_parent', '0'),
(85, 46, '_menu_item_object_id', '38'),
(86, 46, '_menu_item_object', 'page'),
(87, 46, '_menu_item_target', ''),
(88, 46, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(89, 46, '_menu_item_xfn', ''),
(90, 46, '_menu_item_url', ''),
(92, 47, '_menu_item_type', 'post_type'),
(93, 47, '_menu_item_menu_item_parent', '0'),
(94, 47, '_menu_item_object_id', '36'),
(95, 47, '_menu_item_object', 'page'),
(96, 47, '_menu_item_target', ''),
(97, 47, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(98, 47, '_menu_item_xfn', ''),
(99, 47, '_menu_item_url', ''),
(101, 48, '_email', 'project_bf@mail.ru'),
(102, 48, '_name', 'root'),
(103, 48, '_props', 'a:2:{s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";}'),
(104, 48, '_last_contacted', '2021-04-22 16:27:53'),
(105, 49, '_wp_attached_file', '2021/04/logo.svg'),
(106, 49, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:200;s:6:\"height\";i:89;s:4:\"file\";s:17:\"/2021/04/logo.svg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:5:{s:5:\"width\";i:150;s:6:\"height\";i:150;s:4:\"crop\";s:1:\"1\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:6:\"medium\";a:5:{s:5:\"width\";i:300;s:6:\"height\";i:300;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:12:\"medium_large\";a:5:{s:5:\"width\";i:768;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:5:\"large\";a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:9:\"1536x1536\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:9:\"2048x2048\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:12:\"about_us_img\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:11:\"product_img\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:19:\"catalog_product_img\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:16:\"cart_product_img\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:17:\"about_us_page_img\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:8:\"logo.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}}}'),
(107, 49, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:0;s:10:\"size_after\";i:0;s:4:\"time\";i:0;s:11:\"api_version\";i:-1;s:5:\"lossy\";i:-1;s:9:\"keep_exif\";b:0;}s:5:\"sizes\";a:0:{}}'),
(108, 50, '_wp_attached_file', '2021/04/slide-bg.jpg'),
(109, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:669;s:6:\"height\";i:538;s:4:\"file\";s:20:\"2021/04/slide-bg.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"slide-bg-300x241.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:241;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"slide-bg-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"post_news_img\";a:4:{s:4:\"file\";s:20:\"slide-bg-370x227.jpg\";s:5:\"width\";i:370;s:6:\"height\";i:227;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"about_us_img\";a:4:{s:4:\"file\";s:20:\"slide-bg-470x401.jpg\";s:5:\"width\";i:470;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(110, 50, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:4.489155772308816;s:5:\"bytes\";i:4179;s:11:\"size_before\";i:93091;s:10:\"size_after\";i:88912;s:4:\"time\";d:0.54;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:7:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.48;s:5:\"bytes\";i:779;s:11:\"size_before\";i:17372;s:10:\"size_after\";i:16593;s:4:\"time\";d:0.09;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.28;s:5:\"bytes\";i:389;s:11:\"size_before\";i:7368;s:10:\"size_after\";i:6979;s:4:\"time\";d:0.03;}s:12:\"about_us_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.69;s:5:\"bytes\";i:509;s:11:\"size_before\";i:10844;s:10:\"size_after\";i:10335;s:4:\"time\";d:0.04;}s:11:\"product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.6;s:5:\"bytes\";i:487;s:11:\"size_before\";i:10580;s:10:\"size_after\";i:10093;s:4:\"time\";d:0.07;}s:19:\"catalog_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.19;s:5:\"bytes\";i:873;s:11:\"size_before\";i:20834;s:10:\"size_after\";i:19961;s:4:\"time\";d:0.01;}s:16:\"cart_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.81;s:5:\"bytes\";i:371;s:11:\"size_before\";i:7717;s:10:\"size_after\";i:7346;s:4:\"time\";d:0.23;}s:17:\"about_us_page_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.2;s:5:\"bytes\";i:771;s:11:\"size_before\";i:18376;s:10:\"size_after\";i:17605;s:4:\"time\";d:0.07;}}}'),
(111, 7, 'inline_featured_image', '0'),
(112, 7, 'slider_0_title', 'Конкурс-фестиваль “Мы вместе”'),
(113, 7, '_slider_0_title', 'field_60814a958e1f3'),
(114, 7, 'slider_0_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(115, 7, '_slider_0_desc', 'field_60814aa78e1f4'),
(116, 7, 'slider_0_bg', '50'),
(117, 7, '_slider_0_bg', 'field_60814ab28e1f5'),
(118, 7, 'slider_0_btn_link', '#'),
(119, 7, '_slider_0_btn_link', 'field_60814ad58e1f6'),
(120, 7, 'slider_0_btn_text', 'Подробнее'),
(121, 7, '_slider_0_btn_text', 'field_60814ae18e1f7'),
(122, 7, 'slider_1_title', 'Конкурс-фестиваль “Мы вместе”'),
(123, 7, '_slider_1_title', 'field_60814a958e1f3'),
(124, 7, 'slider_1_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(125, 7, '_slider_1_desc', 'field_60814aa78e1f4'),
(126, 7, 'slider_1_bg', '50'),
(127, 7, '_slider_1_bg', 'field_60814ab28e1f5'),
(128, 7, 'slider_1_btn_link', '#'),
(129, 7, '_slider_1_btn_link', 'field_60814ad58e1f6'),
(130, 7, 'slider_1_btn_text', 'Подробнее'),
(131, 7, '_slider_1_btn_text', 'field_60814ae18e1f7'),
(132, 51, 'slider', '2'),
(133, 51, '_slider', 'field_60814a6e8e1f2'),
(134, 51, 'about_us_title', ''),
(135, 51, '_about_us_title', 'field_60814b3c8e1f9'),
(136, 51, 'about_us_desc', ''),
(137, 51, '_about_us_desc', 'field_60814b538e1fa'),
(138, 51, 'about_us_img', ''),
(139, 51, '_about_us_img', 'field_60814b6e8e1fb'),
(140, 51, 'programms_title', ''),
(141, 51, '_programms_title', 'field_60814b9a8e1fd'),
(142, 51, 'programms_items', ''),
(143, 51, '_programms_items', 'field_60814bb08e1fe'),
(144, 51, 'news_title', ''),
(145, 51, '_news_title', 'field_60814bf98e200'),
(146, 51, 'slider_0_title', 'Конкурс-фестиваль “Мы вместе”'),
(147, 51, '_slider_0_title', 'field_60814a958e1f3'),
(148, 51, 'slider_0_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(149, 51, '_slider_0_desc', 'field_60814aa78e1f4'),
(150, 51, 'slider_0_bg', '50'),
(151, 51, '_slider_0_bg', 'field_60814ab28e1f5'),
(152, 51, 'slider_0_btn_link', '#'),
(153, 51, '_slider_0_btn_link', 'field_60814ad58e1f6'),
(154, 51, 'slider_0_btn_text', 'Подробнее'),
(155, 51, '_slider_0_btn_text', 'field_60814ae18e1f7'),
(156, 51, 'slider_1_title', 'Конкурс-фестиваль “Мы вместе”'),
(157, 51, '_slider_1_title', 'field_60814a958e1f3'),
(158, 51, 'slider_1_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(159, 51, '_slider_1_desc', 'field_60814aa78e1f4'),
(160, 51, 'slider_1_bg', '50'),
(161, 51, '_slider_1_bg', 'field_60814ab28e1f5'),
(162, 51, 'slider_1_btn_link', '#'),
(163, 51, '_slider_1_btn_link', 'field_60814ad58e1f6'),
(164, 51, 'slider_1_btn_text', 'Подробнее'),
(165, 51, '_slider_1_btn_text', 'field_60814ae18e1f7'),
(166, 52, '_wp_attached_file', '2021/04/about-us-img.jpg'),
(167, 52, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:470;s:6:\"height\";i:401;s:4:\"file\";s:24:\"2021/04/about-us-img.jpg\";s:5:\"sizes\";a:3:{s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"about-us-img-300x256.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:256;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"about-us-img-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"post_news_img\";a:4:{s:4:\"file\";s:24:\"about-us-img-370x227.jpg\";s:5:\"width\";i:370;s:6:\"height\";i:227;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(168, 53, 'slider', '2'),
(169, 53, '_slider', 'field_60814a6e8e1f2'),
(170, 53, 'about_us_title', 'Бирюков\r\nНиколай Сергеевич'),
(171, 53, '_about_us_title', 'field_60814b3c8e1f9'),
(172, 53, 'about_us_desc', '«МЫ ВМЕСТЕ» ОТ СЛОВ К ДЕЛУ\r\n\r\nОмская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать. Сейчас самое время применить другой подход к людям, считает учредитель Благотворительного Фонда социальных инициатив «Мы вместе», директор ООО «ПЭТСИБ» Николай Бирюков.'),
(173, 53, '_about_us_desc', 'field_60814b538e1fa'),
(174, 53, 'about_us_img', '52'),
(175, 53, '_about_us_img', 'field_60814b6e8e1fb'),
(176, 53, 'programms_title', ''),
(177, 53, '_programms_title', 'field_60814b9a8e1fd'),
(178, 53, 'programms_items', ''),
(179, 53, '_programms_items', 'field_60814bb08e1fe'),
(180, 53, 'news_title', ''),
(181, 53, '_news_title', 'field_60814bf98e200'),
(182, 53, 'slider_0_title', 'Конкурс-фестиваль “Мы вместе”'),
(183, 53, '_slider_0_title', 'field_60814a958e1f3'),
(184, 53, 'slider_0_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(185, 53, '_slider_0_desc', 'field_60814aa78e1f4'),
(186, 53, 'slider_0_bg', '50'),
(187, 53, '_slider_0_bg', 'field_60814ab28e1f5'),
(188, 53, 'slider_0_btn_link', '#'),
(189, 53, '_slider_0_btn_link', 'field_60814ad58e1f6'),
(190, 53, 'slider_0_btn_text', 'Подробнее'),
(191, 53, '_slider_0_btn_text', 'field_60814ae18e1f7'),
(192, 53, 'slider_1_title', 'Конкурс-фестиваль “Мы вместе”'),
(193, 53, '_slider_1_title', 'field_60814a958e1f3'),
(194, 53, 'slider_1_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(195, 53, '_slider_1_desc', 'field_60814aa78e1f4'),
(196, 53, 'slider_1_bg', '50'),
(197, 53, '_slider_1_bg', 'field_60814ab28e1f5'),
(198, 53, 'slider_1_btn_link', '#'),
(199, 53, '_slider_1_btn_link', 'field_60814ad58e1f6'),
(200, 53, 'slider_1_btn_text', 'Подробнее'),
(201, 53, '_slider_1_btn_text', 'field_60814ae18e1f7'),
(202, 52, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:3.0349677261357075;s:5:\"bytes\";i:1735;s:11:\"size_before\";i:57167;s:10:\"size_after\";i:55432;s:4:\"time\";d:0.37;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:7:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:2.46;s:5:\"bytes\";i:248;s:11:\"size_before\";i:10092;s:10:\"size_after\";i:9844;s:4:\"time\";d:0.01;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.81;s:5:\"bytes\";i:176;s:11:\"size_before\";i:4620;s:10:\"size_after\";i:4444;s:4:\"time\";d:0.01;}s:12:\"about_us_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.14;s:5:\"bytes\";i:289;s:11:\"size_before\";i:6973;s:10:\"size_after\";i:6684;s:4:\"time\";d:0.09;}s:11:\"product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.9;s:5:\"bytes\";i:265;s:11:\"size_before\";i:6795;s:10:\"size_after\";i:6530;s:4:\"time\";d:0.01;}s:19:\"catalog_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:2.04;s:5:\"bytes\";i:255;s:11:\"size_before\";i:12480;s:10:\"size_after\";i:12225;s:4:\"time\";d:0.15;}s:16:\"cart_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.01;s:5:\"bytes\";i:204;s:11:\"size_before\";i:5092;s:10:\"size_after\";i:4888;s:4:\"time\";d:0.04;}s:17:\"about_us_page_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:2.68;s:5:\"bytes\";i:298;s:11:\"size_before\";i:11115;s:10:\"size_after\";i:10817;s:4:\"time\";d:0.06;}}}'),
(203, 55, '_edit_last', '1'),
(204, 55, '_edit_lock', '1619088562:1'),
(205, 57, '_edit_last', '1'),
(206, 57, '_edit_lock', '1619088278:1'),
(207, 58, '_wp_attached_file', '2021/04/programm-img1.jpg'),
(208, 58, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:470;s:6:\"height\";i:724;s:4:\"file\";s:25:\"2021/04/programm-img1.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"programm-img1-195x300.jpg\";s:5:\"width\";i:195;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"programm-img1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"post_news_img\";a:4:{s:4:\"file\";s:25:\"programm-img1-370x227.jpg\";s:5:\"width\";i:370;s:6:\"height\";i:227;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"about_us_img\";a:4:{s:4:\"file\";s:25:\"programm-img1-470x401.jpg\";s:5:\"width\";i:470;s:6:\"height\";i:401;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(209, 57, 'img', '58'),
(210, 57, '_img', 'field_6081539e7eacf'),
(211, 58, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:6.277792723647354;s:5:\"bytes\";i:3967;s:11:\"size_before\";i:63191;s:10:\"size_after\";i:59224;s:4:\"time\";d:0.27;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:7:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.44;s:5:\"bytes\";i:609;s:11:\"size_before\";i:9460;s:10:\"size_after\";i:8851;s:4:\"time\";d:0.04;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.54;s:5:\"bytes\";i:376;s:11:\"size_before\";i:5749;s:10:\"size_after\";i:5373;s:4:\"time\";d:0.04;}s:12:\"about_us_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.25;s:5:\"bytes\";i:486;s:11:\"size_before\";i:7780;s:10:\"size_after\";i:7294;s:4:\"time\";d:0.02;}s:11:\"product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.35;s:5:\"bytes\";i:488;s:11:\"size_before\";i:7687;s:10:\"size_after\";i:7199;s:4:\"time\";d:0.05;}s:19:\"catalog_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.98;s:5:\"bytes\";i:845;s:11:\"size_before\";i:14138;s:10:\"size_after\";i:13293;s:4:\"time\";d:0.07;}s:16:\"cart_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.46;s:5:\"bytes\";i:374;s:11:\"size_before\";i:5790;s:10:\"size_after\";i:5416;s:4:\"time\";d:0.04;}s:17:\"about_us_page_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.27;s:5:\"bytes\";i:789;s:11:\"size_before\";i:12587;s:10:\"size_after\";i:11798;s:4:\"time\";d:0.01;}}}'),
(212, 59, '_edit_last', '1'),
(213, 59, '_edit_lock', '1619088314:1'),
(214, 60, '_wp_attached_file', '2021/04/programm-img2.jpg'),
(215, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:670;s:6:\"height\";i:347;s:4:\"file\";s:25:\"2021/04/programm-img2.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"programm-img2-300x155.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"programm-img2-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"post_news_img\";a:4:{s:4:\"file\";s:25:\"programm-img2-370x227.jpg\";s:5:\"width\";i:370;s:6:\"height\";i:227;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"about_us_img\";a:4:{s:4:\"file\";s:25:\"programm-img2-470x347.jpg\";s:5:\"width\";i:470;s:6:\"height\";i:347;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(216, 59, 'img', '60'),
(217, 59, '_img', 'field_6081539e7eacf'),
(218, 60, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:5.33585409252669;s:5:\"bytes\";i:2399;s:11:\"size_before\";i:44960;s:10:\"size_after\";i:42561;s:4:\"time\";d:0.13;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:7:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.22;s:5:\"bytes\";i:324;s:11:\"size_before\";i:6206;s:10:\"size_after\";i:5882;s:4:\"time\";d:0.04;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.21;s:5:\"bytes\";i:214;s:11:\"size_before\";i:4105;s:10:\"size_after\";i:3891;s:4:\"time\";d:0.01;}s:12:\"about_us_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.42;s:5:\"bytes\";i:247;s:11:\"size_before\";i:5587;s:10:\"size_after\";i:5340;s:4:\"time\";d:0.01;}s:11:\"product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.65;s:5:\"bytes\";i:251;s:11:\"size_before\";i:5394;s:10:\"size_after\";i:5143;s:4:\"time\";d:0.01;}s:19:\"catalog_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:5.72;s:5:\"bytes\";i:565;s:11:\"size_before\";i:9880;s:10:\"size_after\";i:9315;s:4:\"time\";d:0.01;}s:16:\"cart_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.96;s:5:\"bytes\";i:219;s:11:\"size_before\";i:4419;s:10:\"size_after\";i:4200;s:4:\"time\";d:0.03;}s:17:\"about_us_page_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.18;s:5:\"bytes\";i:579;s:11:\"size_before\";i:9369;s:10:\"size_after\";i:8790;s:4:\"time\";d:0.02;}}}'),
(219, 61, '_edit_last', '1'),
(220, 61, '_edit_lock', '1619102385:1'),
(221, 62, '_wp_attached_file', '2021/04/programm-img3.jpg'),
(222, 62, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:670;s:6:\"height\";i:347;s:4:\"file\";s:25:\"2021/04/programm-img3.jpg\";s:5:\"sizes\";a:4:{s:6:\"medium\";a:4:{s:4:\"file\";s:25:\"programm-img3-300x155.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:155;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:25:\"programm-img3-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:13:\"post_news_img\";a:4:{s:4:\"file\";s:25:\"programm-img3-370x227.jpg\";s:5:\"width\";i:370;s:6:\"height\";i:227;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"about_us_img\";a:4:{s:4:\"file\";s:25:\"programm-img3-470x347.jpg\";s:5:\"width\";i:470;s:6:\"height\";i:347;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(223, 61, 'img', '62'),
(224, 61, '_img', 'field_6081539e7eacf'),
(225, 62, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:3.675277782623105;s:5:\"bytes\";i:2107;s:11:\"size_before\";i:57329;s:10:\"size_after\";i:55222;s:4:\"time\";d:0.4600000000000001;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:7:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.17;s:5:\"bytes\";i:291;s:11:\"size_before\";i:6983;s:10:\"size_after\";i:6692;s:4:\"time\";d:0.16;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.12;s:5:\"bytes\";i:187;s:11:\"size_before\";i:4539;s:10:\"size_after\";i:4352;s:4:\"time\";d:0.01;}s:12:\"about_us_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:2.8;s:5:\"bytes\";i:197;s:11:\"size_before\";i:7032;s:10:\"size_after\";i:6835;s:4:\"time\";d:0.16;}s:11:\"product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.2;s:5:\"bytes\";i:226;s:11:\"size_before\";i:7057;s:10:\"size_after\";i:6831;s:4:\"time\";d:0.07;}s:19:\"catalog_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.62;s:5:\"bytes\";i:502;s:11:\"size_before\";i:13860;s:10:\"size_after\";i:13358;s:4:\"time\";d:0.02;}s:16:\"cart_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.48;s:5:\"bytes\";i:176;s:11:\"size_before\";i:5057;s:10:\"size_after\";i:4881;s:4:\"time\";d:0.03;}s:17:\"about_us_page_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.12;s:5:\"bytes\";i:528;s:11:\"size_before\";i:12801;s:10:\"size_after\";i:12273;s:4:\"time\";d:0.01;}}}'),
(226, 63, 'slider', '2'),
(227, 63, '_slider', 'field_60814a6e8e1f2'),
(228, 63, 'about_us_title', 'Бирюков\r\nНиколай Сергеевич'),
(229, 63, '_about_us_title', 'field_60814b3c8e1f9'),
(230, 63, 'about_us_desc', '«МЫ ВМЕСТЕ» ОТ СЛОВ К ДЕЛУ\r\n\r\nОмская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать. Сейчас самое время применить другой подход к людям, считает учредитель Благотворительного Фонда социальных инициатив «Мы вместе», директор ООО «ПЭТСИБ» Николай Бирюков.'),
(231, 63, '_about_us_desc', 'field_60814b538e1fa'),
(232, 63, 'about_us_img', '52'),
(233, 63, '_about_us_img', 'field_60814b6e8e1fb'),
(234, 63, 'programms_title', 'Программы'),
(235, 63, '_programms_title', 'field_60814b9a8e1fd'),
(236, 63, 'programms_items', 'a:3:{i:0;s:2:\"61\";i:1;s:2:\"59\";i:2;s:2:\"57\";}'),
(237, 63, '_programms_items', 'field_60814bb08e1fe'),
(238, 63, 'news_title', ''),
(239, 63, '_news_title', 'field_60814bf98e200'),
(240, 63, 'slider_0_title', 'Конкурс-фестиваль “Мы вместе”'),
(241, 63, '_slider_0_title', 'field_60814a958e1f3'),
(242, 63, 'slider_0_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(243, 63, '_slider_0_desc', 'field_60814aa78e1f4'),
(244, 63, 'slider_0_bg', '50'),
(245, 63, '_slider_0_bg', 'field_60814ab28e1f5'),
(246, 63, 'slider_0_btn_link', '#'),
(247, 63, '_slider_0_btn_link', 'field_60814ad58e1f6'),
(248, 63, 'slider_0_btn_text', 'Подробнее'),
(249, 63, '_slider_0_btn_text', 'field_60814ae18e1f7'),
(250, 63, 'slider_1_title', 'Конкурс-фестиваль “Мы вместе”'),
(251, 63, '_slider_1_title', 'field_60814a958e1f3'),
(252, 63, 'slider_1_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(253, 63, '_slider_1_desc', 'field_60814aa78e1f4'),
(254, 63, 'slider_1_bg', '50'),
(255, 63, '_slider_1_bg', 'field_60814ab28e1f5'),
(256, 63, 'slider_1_btn_link', '#'),
(257, 63, '_slider_1_btn_link', 'field_60814ad58e1f6'),
(258, 63, 'slider_1_btn_text', 'Подробнее'),
(259, 63, '_slider_1_btn_text', 'field_60814ae18e1f7'),
(260, 64, 'slider', '2'),
(261, 64, '_slider', 'field_60814a6e8e1f2'),
(262, 64, 'about_us_title', 'Бирюков\r\nНиколай Сергеевич'),
(263, 64, '_about_us_title', 'field_60814b3c8e1f9'),
(264, 64, 'about_us_desc', '«МЫ ВМЕСТЕ» ОТ СЛОВ К ДЕЛУ\r\n\r\nОмская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать. Сейчас самое время применить другой подход к людям, считает учредитель Благотворительного Фонда социальных инициатив «Мы вместе», директор ООО «ПЭТСИБ» Николай Бирюков.'),
(265, 64, '_about_us_desc', 'field_60814b538e1fa'),
(266, 64, 'about_us_img', '52'),
(267, 64, '_about_us_img', 'field_60814b6e8e1fb'),
(268, 64, 'programms_title', 'Программы'),
(269, 64, '_programms_title', 'field_60814b9a8e1fd'),
(270, 64, 'programms_items', 'a:3:{i:0;s:2:\"59\";i:1;s:2:\"61\";i:2;s:2:\"57\";}'),
(271, 64, '_programms_items', 'field_60814bb08e1fe'),
(272, 64, 'news_title', ''),
(273, 64, '_news_title', 'field_60814bf98e200'),
(274, 64, 'slider_0_title', 'Конкурс-фестиваль “Мы вместе”'),
(275, 64, '_slider_0_title', 'field_60814a958e1f3'),
(276, 64, 'slider_0_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(277, 64, '_slider_0_desc', 'field_60814aa78e1f4'),
(278, 64, 'slider_0_bg', '50'),
(279, 64, '_slider_0_bg', 'field_60814ab28e1f5'),
(280, 64, 'slider_0_btn_link', '#'),
(281, 64, '_slider_0_btn_link', 'field_60814ad58e1f6'),
(282, 64, 'slider_0_btn_text', 'Подробнее'),
(283, 64, '_slider_0_btn_text', 'field_60814ae18e1f7'),
(284, 64, 'slider_1_title', 'Конкурс-фестиваль “Мы вместе”'),
(285, 64, '_slider_1_title', 'field_60814a958e1f3'),
(286, 64, 'slider_1_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(287, 64, '_slider_1_desc', 'field_60814aa78e1f4'),
(288, 64, 'slider_1_bg', '50'),
(289, 64, '_slider_1_bg', 'field_60814ab28e1f5'),
(290, 64, 'slider_1_btn_link', '#'),
(291, 64, '_slider_1_btn_link', 'field_60814ad58e1f6'),
(292, 64, 'slider_1_btn_text', 'Подробнее'),
(293, 64, '_slider_1_btn_text', 'field_60814ae18e1f7'),
(294, 65, 'slider', '2'),
(295, 65, '_slider', 'field_60814a6e8e1f2'),
(296, 65, 'about_us_title', 'Бирюков\r\nНиколай Сергеевич'),
(297, 65, '_about_us_title', 'field_60814b3c8e1f9'),
(298, 65, 'about_us_desc', '«МЫ ВМЕСТЕ» ОТ СЛОВ К ДЕЛУ\r\n\r\nОмская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать. Сейчас самое время применить другой подход к людям, считает учредитель Благотворительного Фонда социальных инициатив «Мы вместе», директор ООО «ПЭТСИБ» Николай Бирюков.'),
(299, 65, '_about_us_desc', 'field_60814b538e1fa'),
(300, 65, 'about_us_img', '52'),
(301, 65, '_about_us_img', 'field_60814b6e8e1fb'),
(302, 65, 'programms_title', 'Программы'),
(303, 65, '_programms_title', 'field_60814b9a8e1fd'),
(304, 65, 'programms_items', 'a:3:{i:0;s:2:\"57\";i:1;s:2:\"59\";i:2;s:2:\"61\";}'),
(305, 65, '_programms_items', 'field_60814bb08e1fe'),
(306, 65, 'news_title', ''),
(307, 65, '_news_title', 'field_60814bf98e200'),
(308, 65, 'slider_0_title', 'Конкурс-фестиваль “Мы вместе”'),
(309, 65, '_slider_0_title', 'field_60814a958e1f3'),
(310, 65, 'slider_0_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(311, 65, '_slider_0_desc', 'field_60814aa78e1f4'),
(312, 65, 'slider_0_bg', '50'),
(313, 65, '_slider_0_bg', 'field_60814ab28e1f5'),
(314, 65, 'slider_0_btn_link', '#'),
(315, 65, '_slider_0_btn_link', 'field_60814ad58e1f6'),
(316, 65, 'slider_0_btn_text', 'Подробнее'),
(317, 65, '_slider_0_btn_text', 'field_60814ae18e1f7'),
(318, 65, 'slider_1_title', 'Конкурс-фестиваль “Мы вместе”'),
(319, 65, '_slider_1_title', 'field_60814a958e1f3'),
(320, 65, 'slider_1_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(321, 65, '_slider_1_desc', 'field_60814aa78e1f4'),
(322, 65, 'slider_1_bg', '50'),
(323, 65, '_slider_1_bg', 'field_60814ab28e1f5'),
(324, 65, 'slider_1_btn_link', '#'),
(325, 65, '_slider_1_btn_link', 'field_60814ad58e1f6'),
(326, 65, 'slider_1_btn_text', 'Подробнее'),
(327, 65, '_slider_1_btn_text', 'field_60814ae18e1f7'),
(328, 66, 'inline_featured_image', '0'),
(329, 66, '_edit_lock', '1619091231:1'),
(330, 67, '_wp_attached_file', '2021/04/news-img.jpg'),
(331, 67, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:370;s:6:\"height\";i:227;s:4:\"file\";s:20:\"2021/04/news-img.jpg\";s:5:\"sizes\";a:2:{s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"news-img-300x184.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:184;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"news-img-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(332, 67, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:5.732233116817327;s:5:\"bytes\";i:5092;s:11:\"size_before\";i:88831;s:10:\"size_after\";i:83739;s:4:\"time\";d:0.25;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:7:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.17;s:5:\"bytes\";i:912;s:11:\"size_before\";i:14787;s:10:\"size_after\";i:13875;s:4:\"time\";d:0.08;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.12;s:5:\"bytes\";i:422;s:11:\"size_before\";i:6900;s:10:\"size_after\";i:6478;s:4:\"time\";d:0.01;}s:12:\"about_us_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.64;s:5:\"bytes\";i:824;s:11:\"size_before\";i:12417;s:10:\"size_after\";i:11593;s:4:\"time\";d:0.01;}s:11:\"product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:11593;s:10:\"size_after\";i:11593;s:4:\"time\";d:0.02;}s:19:\"catalog_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.92;s:5:\"bytes\";i:1276;s:11:\"size_before\";i:18431;s:10:\"size_after\";i:17155;s:4:\"time\";d:0.01;}s:16:\"cart_product_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.67;s:5:\"bytes\";i:520;s:11:\"size_before\";i:7796;s:10:\"size_after\";i:7276;s:4:\"time\";d:0.06;}s:17:\"about_us_page_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.73;s:5:\"bytes\";i:1138;s:11:\"size_before\";i:16907;s:10:\"size_after\";i:15769;s:4:\"time\";d:0.06;}}}'),
(334, 66, '_thumbnail_id', '67'),
(335, 69, 'inline_featured_image', '0'),
(336, 69, 'inline_featured_image', '0'),
(337, 69, '_edit_lock', '1619091122:1'),
(338, 69, '_thumbnail_id', '67'),
(340, 69, '_wp_old_slug', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas'),
(341, 71, 'inline_featured_image', '0'),
(342, 71, '_edit_lock', '1619091380:1'),
(343, 72, '_edit_last', '1'),
(344, 72, '_edit_lock', '1619093528:1'),
(347, 71, '_edit_last', '1'),
(349, 71, 'news_time', '17:00:00'),
(350, 71, '_news_time', 'field_60815f628044b'),
(351, 75, 'news_time', '17:00:00'),
(352, 75, '_news_time', 'field_60815f628044b'),
(353, 76, 'inline_featured_image', '0'),
(354, 76, 'inline_featured_image', '0'),
(355, 76, '_edit_lock', '1619091390:1'),
(356, 76, '_edit_last', '1'),
(358, 76, 'news_time', '17:00:00'),
(359, 76, '_news_time', 'field_60815f628044b'),
(361, 76, '_wp_old_slug', 'v-forum-endaumenty-2021-bolshe-chem-dengi'),
(362, 78, 'inline_featured_image', '0'),
(363, 78, '_edit_lock', '1619091510:1'),
(364, 79, '_edit_last', '1'),
(365, 79, '_edit_lock', '1619093447:1'),
(367, 78, '_edit_last', '1'),
(369, 78, 'news_source', '“Коммерческие вести”'),
(370, 78, '_news_source', 'field_60815feacad5a'),
(371, 81, 'news_source', ''),
(372, 81, '_news_source', 'field_60815feacad5a'),
(374, 82, 'news_source', '“Коммерческие вести”'),
(375, 82, '_news_source', 'field_60815feacad5a'),
(376, 83, 'inline_featured_image', '0'),
(377, 83, 'inline_featured_image', '0'),
(378, 83, '_edit_lock', '1619091545:1'),
(379, 83, '_edit_last', '1'),
(380, 83, 'news_source', '“Коммерческие вести”'),
(381, 83, '_news_source', 'field_60815feacad5a'),
(384, 84, 'inline_featured_image', '0'),
(385, 84, 'inline_featured_image', '0'),
(386, 84, 'inline_featured_image', '0'),
(387, 84, '_edit_lock', '1619091535:1'),
(388, 84, '_edit_last', '1'),
(389, 84, 'news_source', '“Коммерческие вести”'),
(390, 84, '_news_source', 'field_60815feacad5a'),
(392, 85, 'inline_featured_image', '0'),
(393, 85, 'inline_featured_image', '0'),
(394, 85, 'inline_featured_image', '0'),
(395, 85, 'inline_featured_image', '0'),
(396, 85, '_edit_lock', '1619091530:1'),
(397, 85, '_edit_last', '1'),
(398, 85, 'news_source', '“Коммерческие вести”'),
(399, 85, '_news_source', 'field_60815feacad5a'),
(408, 86, 'inline_featured_image', '0'),
(409, 86, 'inline_featured_image', '0'),
(410, 86, 'inline_featured_image', '0'),
(411, 86, 'inline_featured_image', '0'),
(412, 86, 'inline_featured_image', '0'),
(413, 86, '_edit_lock', '1619102531:1'),
(414, 86, '_edit_last', '1'),
(415, 86, 'news_source', '“Коммерческие вести”'),
(416, 86, '_news_source', 'field_60815feacad5a'),
(424, 86, '_wp_old_slug', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas-3'),
(425, 85, '_wp_old_slug', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas-3'),
(426, 84, '_wp_old_slug', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas-3'),
(427, 83, '_wp_old_slug', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas-3'),
(430, 91, 'news_source', '“Коммерческие вести”'),
(431, 91, '_news_source', 'field_60815feacad5a'),
(432, 86, '_encloseme', '1'),
(433, 92, 'news_source', '“Коммерческие вести”'),
(434, 92, '_news_source', 'field_60815feacad5a'),
(435, 94, '_wp_attached_file', '2021/04/youtube-icon.svg'),
(436, 94, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:28;s:6:\"height\";i:28;s:4:\"file\";s:25:\"/2021/04/youtube-icon.svg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:5:{s:5:\"width\";i:150;s:6:\"height\";i:150;s:4:\"crop\";s:1:\"1\";s:4:\"file\";s:16:\"youtube-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:6:\"medium\";a:5:{s:5:\"width\";i:300;s:6:\"height\";i:300;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:16:\"youtube-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:12:\"medium_large\";a:5:{s:5:\"width\";i:768;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:16:\"youtube-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:5:\"large\";a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:16:\"youtube-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:9:\"1536x1536\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:16:\"youtube-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:9:\"2048x2048\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:16:\"youtube-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:13:\"post_news_img\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:16:\"youtube-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}}}'),
(437, 94, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:0;s:10:\"size_after\";i:0;s:4:\"time\";i:0;s:11:\"api_version\";i:-1;s:5:\"lossy\";i:-1;s:9:\"keep_exif\";b:0;}s:5:\"sizes\";a:0:{}}'),
(438, 95, '_wp_attached_file', '2021/04/inst-icon.svg'),
(439, 95, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:28;s:6:\"height\";i:28;s:4:\"file\";s:22:\"/2021/04/inst-icon.svg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:5:{s:5:\"width\";i:150;s:6:\"height\";i:150;s:4:\"crop\";s:1:\"1\";s:4:\"file\";s:13:\"inst-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:6:\"medium\";a:5:{s:5:\"width\";i:300;s:6:\"height\";i:300;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:13:\"inst-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:12:\"medium_large\";a:5:{s:5:\"width\";i:768;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:13:\"inst-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:5:\"large\";a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:13:\"inst-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:9:\"1536x1536\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:13:\"inst-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:9:\"2048x2048\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:13:\"inst-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:13:\"post_news_img\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:13:\"inst-icon.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}}}'),
(440, 95, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:0;s:10:\"size_after\";i:0;s:4:\"time\";i:0;s:11:\"api_version\";i:-1;s:5:\"lossy\";i:-1;s:9:\"keep_exif\";b:0;}s:5:\"sizes\";a:0:{}}'),
(441, 96, '_wp_attached_file', '2021/04/arrow-prev.svg'),
(442, 96, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:34;s:6:\"height\";i:34;s:4:\"file\";s:23:\"/2021/04/arrow-prev.svg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:5:{s:5:\"width\";i:150;s:6:\"height\";i:150;s:4:\"crop\";s:1:\"1\";s:4:\"file\";s:14:\"arrow-prev.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:6:\"medium\";a:5:{s:5:\"width\";i:300;s:6:\"height\";i:300;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-prev.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:12:\"medium_large\";a:5:{s:5:\"width\";i:768;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-prev.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:5:\"large\";a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-prev.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:9:\"1536x1536\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-prev.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:9:\"2048x2048\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-prev.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:13:\"post_news_img\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-prev.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}}}'),
(443, 96, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:0;s:10:\"size_after\";i:0;s:4:\"time\";i:0;s:11:\"api_version\";i:-1;s:5:\"lossy\";i:-1;s:9:\"keep_exif\";b:0;}s:5:\"sizes\";a:0:{}}'),
(444, 97, '_wp_attached_file', '2021/04/arrow-next.svg'),
(445, 97, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:34;s:6:\"height\";i:34;s:4:\"file\";s:23:\"/2021/04/arrow-next.svg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:5:{s:5:\"width\";i:150;s:6:\"height\";i:150;s:4:\"crop\";s:1:\"1\";s:4:\"file\";s:14:\"arrow-next.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:6:\"medium\";a:5:{s:5:\"width\";i:300;s:6:\"height\";i:300;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-next.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:12:\"medium_large\";a:5:{s:5:\"width\";i:768;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-next.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:5:\"large\";a:5:{s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-next.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:9:\"1536x1536\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-next.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:9:\"2048x2048\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-next.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}s:13:\"post_news_img\";a:5:{s:5:\"width\";i:0;s:6:\"height\";i:0;s:4:\"crop\";s:1:\"0\";s:4:\"file\";s:14:\"arrow-next.svg\";s:9:\"mime-type\";s:13:\"image/svg+xml\";}}}'),
(446, 97, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";i:0;s:5:\"bytes\";i:0;s:11:\"size_before\";i:0;s:10:\"size_after\";i:0;s:4:\"time\";i:0;s:11:\"api_version\";i:-1;s:5:\"lossy\";i:-1;s:9:\"keep_exif\";b:0;}s:5:\"sizes\";a:0:{}}'),
(447, 98, 'slider', '2'),
(448, 98, '_slider', 'field_60814a6e8e1f2'),
(449, 98, 'about_us_title', 'Бирюков\r\nНиколай Сергеевич'),
(450, 98, '_about_us_title', 'field_60814b3c8e1f9'),
(451, 98, 'about_us_desc', '<strong>«МЫ ВМЕСТЕ» ОТ СЛОВ К ДЕЛУ</strong>\r\n\r\nОмская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать. Сейчас самое время применить другой подход к людям, считает учредитель Благотворительного Фонда социальных инициатив «Мы вместе», директор ООО «ПЭТСИБ» Николай Бирюков.'),
(452, 98, '_about_us_desc', 'field_60814b538e1fa'),
(453, 98, 'about_us_img', '52'),
(454, 98, '_about_us_img', 'field_60814b6e8e1fb'),
(455, 98, 'programms_title', 'Программы'),
(456, 98, '_programms_title', 'field_60814b9a8e1fd'),
(457, 98, 'programms_items', 'a:3:{i:0;s:2:\"57\";i:1;s:2:\"59\";i:2;s:2:\"61\";}'),
(458, 98, '_programms_items', 'field_60814bb08e1fe'),
(459, 98, 'news_title', ''),
(460, 98, '_news_title', 'field_60814bf98e200'),
(461, 98, 'slider_0_title', 'Конкурс-фестиваль “Мы вместе”'),
(462, 98, '_slider_0_title', 'field_60814a958e1f3'),
(463, 98, 'slider_0_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(464, 98, '_slider_0_desc', 'field_60814aa78e1f4'),
(465, 98, 'slider_0_bg', '50'),
(466, 98, '_slider_0_bg', 'field_60814ab28e1f5'),
(467, 98, 'slider_0_btn_link', '#'),
(468, 98, '_slider_0_btn_link', 'field_60814ad58e1f6'),
(469, 98, 'slider_0_btn_text', 'Подробнее'),
(470, 98, '_slider_0_btn_text', 'field_60814ae18e1f7'),
(471, 98, 'slider_1_title', 'Конкурс-фестиваль “Мы вместе”'),
(472, 98, '_slider_1_title', 'field_60814a958e1f3'),
(473, 98, 'slider_1_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(474, 98, '_slider_1_desc', 'field_60814aa78e1f4'),
(475, 98, 'slider_1_bg', '50'),
(476, 98, '_slider_1_bg', 'field_60814ab28e1f5'),
(477, 98, 'slider_1_btn_link', '#'),
(478, 98, '_slider_1_btn_link', 'field_60814ad58e1f6'),
(479, 98, 'slider_1_btn_text', 'Подробнее'),
(480, 98, '_slider_1_btn_text', 'field_60814ae18e1f7'),
(481, 99, 'slider', '2'),
(482, 99, '_slider', 'field_60814a6e8e1f2'),
(483, 99, 'about_us_title', 'Бирюков\r\nНиколай Сергеевич'),
(484, 99, '_about_us_title', 'field_60814b3c8e1f9'),
(485, 99, 'about_us_desc', '<h5>«МЫ ВМЕСТЕ» ОТ СЛОВ К ДЕЛУ</h5>\r\nОмская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать. Сейчас самое время применить другой подход к людям, считает учредитель Благотворительного Фонда социальных инициатив «Мы вместе», директор ООО «ПЭТСИБ» Николай Бирюков.'),
(486, 99, '_about_us_desc', 'field_60814b538e1fa'),
(487, 99, 'about_us_img', '52'),
(488, 99, '_about_us_img', 'field_60814b6e8e1fb'),
(489, 99, 'programms_title', 'Программы'),
(490, 99, '_programms_title', 'field_60814b9a8e1fd'),
(491, 99, 'programms_items', 'a:3:{i:0;s:2:\"57\";i:1;s:2:\"59\";i:2;s:2:\"61\";}'),
(492, 99, '_programms_items', 'field_60814bb08e1fe'),
(493, 99, 'news_title', ''),
(494, 99, '_news_title', 'field_60814bf98e200'),
(495, 99, 'slider_0_title', 'Конкурс-фестиваль “Мы вместе”'),
(496, 99, '_slider_0_title', 'field_60814a958e1f3'),
(497, 99, 'slider_0_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(498, 99, '_slider_0_desc', 'field_60814aa78e1f4'),
(499, 99, 'slider_0_bg', '50'),
(500, 99, '_slider_0_bg', 'field_60814ab28e1f5'),
(501, 99, 'slider_0_btn_link', '#'),
(502, 99, '_slider_0_btn_link', 'field_60814ad58e1f6'),
(503, 99, 'slider_0_btn_text', 'Подробнее'),
(504, 99, '_slider_0_btn_text', 'field_60814ae18e1f7'),
(505, 99, 'slider_1_title', 'Конкурс-фестиваль “Мы вместе”'),
(506, 99, '_slider_1_title', 'field_60814a958e1f3'),
(507, 99, 'slider_1_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(508, 99, '_slider_1_desc', 'field_60814aa78e1f4'),
(509, 99, 'slider_1_bg', '50'),
(510, 99, '_slider_1_bg', 'field_60814ab28e1f5'),
(511, 99, 'slider_1_btn_link', '#');
INSERT INTO `wp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(512, 99, '_slider_1_btn_link', 'field_60814ad58e1f6'),
(513, 99, 'slider_1_btn_text', 'Подробнее'),
(514, 99, '_slider_1_btn_text', 'field_60814ae18e1f7'),
(515, 100, 'slider', '2'),
(516, 100, '_slider', 'field_60814a6e8e1f2'),
(517, 100, 'about_us_title', 'Бирюков\r\nНиколай Сергеевич'),
(518, 100, '_about_us_title', 'field_60814b3c8e1f9'),
(519, 100, 'about_us_desc', '<h5>«МЫ ВМЕСТЕ» ОТ СЛОВ К ДЕЛУ</h5>\r\nОмская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать. Сейчас самое время применить другой подход к людям, считает учредитель Благотворительного Фонда социальных инициатив «Мы вместе», директор ООО «ПЭТСИБ» Николай Бирюков.'),
(520, 100, '_about_us_desc', 'field_60814b538e1fa'),
(521, 100, 'about_us_img', '52'),
(522, 100, '_about_us_img', 'field_60814b6e8e1fb'),
(523, 100, 'programms_title', 'Программы'),
(524, 100, '_programms_title', 'field_60814b9a8e1fd'),
(525, 100, 'programms_items', 'a:3:{i:0;s:2:\"57\";i:1;s:2:\"59\";i:2;s:2:\"61\";}'),
(526, 100, '_programms_items', 'field_60814bb08e1fe'),
(527, 100, 'news_title', 'Пресс-центр'),
(528, 100, '_news_title', 'field_60814bf98e200'),
(529, 100, 'slider_0_title', 'Конкурс-фестиваль “Мы вместе”'),
(530, 100, '_slider_0_title', 'field_60814a958e1f3'),
(531, 100, 'slider_0_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(532, 100, '_slider_0_desc', 'field_60814aa78e1f4'),
(533, 100, 'slider_0_bg', '50'),
(534, 100, '_slider_0_bg', 'field_60814ab28e1f5'),
(535, 100, 'slider_0_btn_link', '#'),
(536, 100, '_slider_0_btn_link', 'field_60814ad58e1f6'),
(537, 100, 'slider_0_btn_text', 'Подробнее'),
(538, 100, '_slider_0_btn_text', 'field_60814ae18e1f7'),
(539, 100, 'slider_1_title', 'Конкурс-фестиваль “Мы вместе”'),
(540, 100, '_slider_1_title', 'field_60814a958e1f3'),
(541, 100, 'slider_1_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(542, 100, '_slider_1_desc', 'field_60814aa78e1f4'),
(543, 100, 'slider_1_bg', '50'),
(544, 100, '_slider_1_bg', 'field_60814ab28e1f5'),
(545, 100, 'slider_1_btn_link', '#'),
(546, 100, '_slider_1_btn_link', 'field_60814ad58e1f6'),
(547, 100, 'slider_1_btn_text', 'Подробнее'),
(548, 100, '_slider_1_btn_text', 'field_60814ae18e1f7'),
(549, 38, '_edit_last', '1'),
(550, 38, '_wp_page_template', 'default'),
(551, 38, 'inline_featured_image', '0'),
(552, 40, '_edit_last', '1'),
(553, 40, '_wp_page_template', 'default'),
(554, 40, 'inline_featured_image', '0'),
(555, 42, 'inline_featured_image', '0'),
(556, 36, 'inline_featured_image', '0'),
(557, 105, 'slider', '2'),
(558, 105, '_slider', 'field_60814a6e8e1f2'),
(559, 105, 'about_us_title', 'Бирюков\r\nНиколай Сергеевич'),
(560, 105, '_about_us_title', 'field_60814b3c8e1f9'),
(561, 105, 'about_us_desc', '<h5>«МЫ ВМЕСТЕ» ОТ СЛОВ К ДЕЛУ</h5>\r\nОмская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать. Сейчас самое время применить другой подход к людям, считает учредитель Благотворительного Фонда социальных инициатив «Мы вместе», директор ООО «ПЭТСИБ» Николай Бирюков.'),
(562, 105, '_about_us_desc', 'field_60814b538e1fa'),
(563, 105, 'about_us_img', '52'),
(564, 105, '_about_us_img', 'field_60814b6e8e1fb'),
(565, 105, 'programms_title', 'Программы'),
(566, 105, '_programms_title', 'field_60814b9a8e1fd'),
(567, 105, 'programms_items', 'a:3:{i:0;s:2:\"57\";i:1;s:2:\"59\";i:2;s:2:\"61\";}'),
(568, 105, '_programms_items', 'field_60814bb08e1fe'),
(569, 105, 'news_title', 'Пресс-центр'),
(570, 105, '_news_title', 'field_60814bf98e200'),
(571, 105, 'slider_0_title', 'Конкурс-фестиваль “Мы вместе”'),
(572, 105, '_slider_0_title', 'field_60814a958e1f3'),
(573, 105, 'slider_0_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(574, 105, '_slider_0_desc', 'field_60814aa78e1f4'),
(575, 105, 'slider_0_bg', '50'),
(576, 105, '_slider_0_bg', 'field_60814ab28e1f5'),
(577, 105, 'slider_0_btn_link', '#'),
(578, 105, '_slider_0_btn_link', 'field_60814ad58e1f6'),
(579, 105, 'slider_0_btn_text', 'Подробнее'),
(580, 105, '_slider_0_btn_text', 'field_60814ae18e1f7'),
(581, 105, 'slider_1_title', 'Конкурс-фестиваль “Мы вместе”'),
(582, 105, '_slider_1_title', 'field_60814a958e1f3'),
(583, 105, 'slider_1_desc', 'Омская область богата креативными и талантливыми людьми. Они могут получить поддержку на городском и областном уровнях, однако, для реализации проектов омичи продолжают уезжать.'),
(584, 105, '_slider_1_desc', 'field_60814aa78e1f4'),
(585, 105, 'slider_1_bg', '50'),
(586, 105, '_slider_1_bg', 'field_60814ab28e1f5'),
(587, 105, 'slider_1_btn_link', '#'),
(588, 105, '_slider_1_btn_link', 'field_60814ad58e1f6'),
(589, 105, 'slider_1_btn_text', 'Подробнее'),
(590, 105, '_slider_1_btn_text', 'field_60814ae18e1f7'),
(591, 106, '_wp_attached_file', '2021/04/favicon.png'),
(592, 106, '_wp_attachment_metadata', 'a:4:{s:5:\"width\";i:65;s:6:\"height\";i:64;s:4:\"file\";s:19:\"2021/04/favicon.png\";s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(593, 106, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:42.13991769547325;s:5:\"bytes\";i:1536;s:11:\"size_before\";i:3645;s:10:\"size_after\";i:2109;s:4:\"time\";d:0.02;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:1:{s:4:\"full\";O:8:\"stdClass\":5:{s:7:\"percent\";d:42.14;s:5:\"bytes\";i:1536;s:11:\"size_before\";i:3645;s:10:\"size_after\";i:2109;s:4:\"time\";d:0.02;}}}'),
(594, 107, '_wp_attached_file', '2021/04/cropped-favicon.png'),
(595, 107, '_wp_attachment_context', 'site-icon'),
(596, 107, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:512;s:6:\"height\";i:512;s:4:\"file\";s:27:\"2021/04/cropped-favicon.png\";s:5:\"sizes\";a:8:{s:6:\"medium\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:9:\"thumbnail\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"post_news_img\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-370x227.png\";s:5:\"width\";i:370;s:6:\"height\";i:227;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"about_us_img\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-470x401.png\";s:5:\"width\";i:470;s:6:\"height\";i:401;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-270\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-270x270.png\";s:5:\"width\";i:270;s:6:\"height\";i:270;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-192\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-192x192.png\";s:5:\"width\";i:192;s:6:\"height\";i:192;s:9:\"mime-type\";s:9:\"image/png\";}s:13:\"site_icon-180\";a:4:{s:4:\"file\";s:27:\"cropped-favicon-180x180.png\";s:5:\"width\";i:180;s:6:\"height\";i:180;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"site_icon-32\";a:4:{s:4:\"file\";s:25:\"cropped-favicon-32x32.png\";s:5:\"width\";i:32;s:6:\"height\";i:32;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:11:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";}}'),
(597, 108, '_wp_trash_meta_status', 'publish'),
(598, 108, '_wp_trash_meta_time', '1619159511'),
(599, 107, 'wp-smpro-smush-data', 'a:2:{s:5:\"stats\";a:8:{s:7:\"percent\";d:8.636895591372364;s:5:\"bytes\";i:23321;s:11:\"size_before\";i:270016;s:10:\"size_after\";i:246695;s:4:\"time\";d:1.06;s:11:\"api_version\";s:3:\"1.0\";s:5:\"lossy\";b:0;s:9:\"keep_exif\";i:0;}s:5:\"sizes\";a:8:{s:6:\"medium\";O:8:\"stdClass\":5:{s:7:\"percent\";d:6.11;s:5:\"bytes\";i:2908;s:11:\"size_before\";i:47612;s:10:\"size_after\";i:44704;s:4:\"time\";d:0.13;}s:9:\"thumbnail\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.11;s:5:\"bytes\";i:573;s:11:\"size_before\";i:18420;s:10:\"size_after\";i:17847;s:4:\"time\";d:0.04;}s:13:\"post_news_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:9.26;s:5:\"bytes\";i:4291;s:11:\"size_before\";i:46341;s:10:\"size_after\";i:42050;s:4:\"time\";d:0.12;}s:12:\"about_us_img\";O:8:\"stdClass\":5:{s:7:\"percent\";d:13.77;s:5:\"bytes\";i:11254;s:11:\"size_before\";i:81748;s:10:\"size_after\";i:70494;s:4:\"time\";d:0.42;}s:13:\"site_icon-270\";O:8:\"stdClass\":5:{s:7:\"percent\";d:4.09;s:5:\"bytes\";i:1602;s:11:\"size_before\";i:39137;s:10:\"size_after\";i:37535;s:4:\"time\";d:0.19;}s:13:\"site_icon-192\";O:8:\"stdClass\":5:{s:7:\"percent\";d:11.32;s:5:\"bytes\";i:1355;s:11:\"size_before\";i:11974;s:10:\"size_after\";i:10619;s:4:\"time\";d:0.06;}s:13:\"site_icon-180\";O:8:\"stdClass\":5:{s:7:\"percent\";d:3.66;s:5:\"bytes\";i:838;s:11:\"size_before\";i:22897;s:10:\"size_after\";i:22059;s:4:\"time\";d:0.08;}s:12:\"site_icon-32\";O:8:\"stdClass\":5:{s:7:\"percent\";d:26.5;s:5:\"bytes\";i:500;s:11:\"size_before\";i:1887;s:10:\"size_after\";i:1387;s:4:\"time\";d:0.02;}}}');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_posts`
--

CREATE TABLE `wp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_posts`
--

INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(3, 1, '2021-04-22 12:38:51', '2021-04-22 09:38:51', '<!-- wp:heading --><h2>Кто мы</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Предлагаемый текст: </strong>Наш адрес сайта: http://wt.iilw.ru.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Комментарии</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Предлагаемый текст: </strong>Если посетитель оставляет комментарий на сайте, мы собираем данные указанные в форме комментария, а также IP адрес посетителя и данные user-agent браузера с целью определения спама.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Анонимизированная строка создаваемая из вашего адреса email (\"хеш\") может предоставляться сервису Gravatar, чтобы определить используете ли вы его. Политика конфиденциальности Gravatar доступна здесь: https://automattic.com/privacy/ . После одобрения комментария ваше изображение профиля будет видимым публично в контексте вашего комментария.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Медиафайлы</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Предлагаемый текст: </strong>Если вы зарегистрированный пользователь и загружаете фотографии на сайт, вам возможно следует избегать загрузки изображений с метаданными EXIF, так как они могут содержать данные вашего месторасположения по GPS. Посетители могут извлечь эту информацию скачав изображения с сайта.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Куки</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Предлагаемый текст: </strong>Если вы оставляете комментарий на нашем сайте, вы можете включить сохранение вашего имени, адреса email и вебсайта в куки. Это делается для вашего удобства, чтобы не заполнять данные снова при повторном комментировании. Эти куки хранятся в течение одного года.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Если у вас есть учетная запись на сайте и вы войдете в неё, мы установим временный куки для определения поддержки куки вашим браузером, куки не содержит никакой личной информации и удаляется при закрытии вашего браузера.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>При входе в учетную запись мы также устанавливаем несколько куки с данными входа и настройками экрана. Куки входа хранятся в течение двух дней, куки с настройками экрана - год. Если вы выберете возможность \"Запомнить меня\", данные о входе будут сохраняться в течение двух недель. При выходе из учетной записи куки входа будут удалены.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>При редактировании или публикации статьи в браузере будет сохранен дополнительный куки, он не содержит персональных данных и содержит только ID записи отредактированной вами, истекает через 1 день.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Встраиваемое содержимое других вебсайтов</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Предлагаемый текст: </strong>Статьи на этом сайте могут включать встраиваемое содержимое (например видео, изображения, статьи и др.), подобное содержимое ведет себя так же, как если бы посетитель зашел на другой сайт.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Эти сайты могут собирать данные о вас, использовать куки, внедрять дополнительное отслеживание третьей стороной и следить за вашим взаимодействием с внедренным содержимым, включая отслеживание взаимодействия, если у вас есть учетная запись и вы авторизовались на том сайте.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>С кем мы делимся вашими данными</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Предлагаемый текст: </strong>Если вы запросите сброс пароля, ваш IP будет указан в email-сообщении о сбросе.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Как долго мы храним ваши данные</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Предлагаемый текст: </strong>Если вы оставляете комментарий, то сам комментарий и его метаданные сохраняются неопределенно долго. Это делается для того, чтобы определять и одобрять последующие комментарии автоматически, вместо помещения их в очередь на одобрение.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Для пользователей с регистрацией на нашем сайте мы храним ту личную информацию, которую они указывают в своем профиле. Все пользователи могут видеть, редактировать или удалить свою информацию из профиля в любое время (кроме имени пользователя). Администрация вебсайта также может видеть и изменять эту информацию.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Какие у вас права на ваши данные</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Предлагаемый текст: </strong>При наличии учетной записи на сайте или если вы оставляли комментарии, то вы можете запросить файл экспорта персональных данных, которые мы сохранили о вас, включая предоставленные вами данные. Вы также можете запросить удаление этих данных, это не включает данные, которые мы обязаны хранить в административных целях, по закону или целях безопасности.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Куда мы отправляем ваши данные</h2><!-- /wp:heading --><!-- wp:paragraph --><p><strong class=\"privacy-policy-tutorial\">Предлагаемый текст: </strong>Комментарии пользователей могут проверяться автоматическим сервисом определения спама.</p><!-- /wp:paragraph -->', 'Политика конфиденциальности', '', 'draft', 'closed', 'open', '', 'privacy-policy', '', '', '2021-04-22 12:38:51', '2021-04-22 09:38:51', '', 0, 'http://wt.iilw.ru/?page_id=3', 0, 'page', '', 0),
(4, 1, '2021-04-22 12:39:03', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'open', 'open', '', '', '', '', '2021-04-22 12:39:03', '0000-00-00 00:00:00', '', 0, 'http://wt.iilw.ru/?p=4', 0, 'post', '', 0),
(7, 1, '2021-04-22 12:39:40', '2021-04-22 09:39:40', '', 'Главная страница', '', 'publish', 'closed', 'closed', '', 'glavnaya-stranicza', '', '', '2021-04-23 01:47:24', '2021-04-22 19:47:24', '', 0, 'http://wt.iilw.ru/?page_id=7', 0, 'page', '', 0),
(8, 1, '2021-04-22 12:39:40', '2021-04-22 09:39:40', '', 'Главная страница', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-04-22 12:39:40', '2021-04-22 09:39:40', '', 7, 'http://wt.iilw.ru/?p=8', 0, 'revision', '', 0),
(9, 1, '2021-04-22 12:40:11', '2021-04-22 09:40:11', '<label> Your name\n    [text* your-name] </label>\n\n<label> Your email\n    [email* your-email] </label>\n\n<label> Тема\n    [text* your-subject] </label>\n\n<label> Your message (optional)\n    [textarea your-message] </label>\n\n[submit \"Submit\"]\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@wt.iilw.ru>\nОт: [your-name] <[your-email]>\nТема: [your-subject]\n\nСообщение:\n[your-message]\n\n-- \nЭто сообщение отправлено с сайта [_site_title] ([_site_url])\n[_site_admin_email]\nReply-To: [your-email]\n\n0\n0\n\n[_site_title] \"[your-subject]\"\n[_site_title] <wordpress@wt.iilw.ru>\nСообщение:\n[your-message]\n\n-- \nЭто сообщение отправлено с сайта [_site_title] ([_site_url])\n[your-email]\nReply-To: [_site_admin_email]\n\n0\n0\nСпасибо за Ваше сообщение. Оно успешно отправлено.\nПри отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\nОдно или несколько полей содержат ошибочные данные. Пожалуйста, проверьте их и попробуйте ещё раз.\nПри отправке сообщения произошла ошибка. Пожалуйста, попробуйте ещё раз позже.\nВы должны принять условия и положения перед отправкой вашего сообщения.\nПоле обязательно для заполнения.\nПоле слишком длинное.\nПоле слишком короткое.\nПри загрузке файла произошла неизвестная ошибка.\nВам не разрешено загружать файлы этого типа.\nФайл слишком большой.\nПри загрузке файла произошла ошибка.', 'Контактная форма 1', '', 'publish', 'closed', 'closed', '', '%d0%ba%d0%be%d0%bd%d1%82%d0%b0%d0%ba%d1%82%d0%bd%d0%b0%d1%8f-%d1%84%d0%be%d1%80%d0%bc%d0%b0-1', '', '', '2021-04-22 12:40:11', '2021-04-22 09:40:11', '', 0, 'http://wt.iilw.ru/?post_type=wpcf7_contact_form&p=9', 0, 'wpcf7_contact_form', '', 0),
(10, 1, '2021-04-22 15:59:06', '2021-04-22 09:59:06', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:12:\"options_page\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:18:\"theme-add-settings\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Для основных настроек темы', 'dlya-osnovnyh-nastroek-temy', 'publish', 'closed', 'closed', '', 'group_6081485d21ae7', '', '', '2021-04-22 18:15:36', '2021-04-22 12:15:36', '', 0, 'http://wt.iilw.ru/?post_type=acf-field-group&#038;p=10', 0, 'acf-field-group', '', 0),
(11, 1, '2021-04-22 15:59:06', '2021-04-22 09:59:06', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:4:\"left\";s:8:\"endpoint\";i:0;}', 'Основные', 'osnovnye', 'publish', 'closed', 'closed', '', 'field_60814867ce81b', '', '', '2021-04-22 15:59:06', '2021-04-22 09:59:06', '', 10, 'http://wt.iilw.ru/?post_type=acf-field&p=11', 0, 'acf-field', '', 0),
(12, 1, '2021-04-22 15:59:06', '2021-04-22 09:59:06', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Логотип', 'logo', 'publish', 'closed', 'closed', '', 'field_6081487bce81c', '', '', '2021-04-22 15:59:06', '2021-04-22 09:59:06', '', 10, 'http://wt.iilw.ru/?post_type=acf-field&p=12', 1, 'acf-field', '', 0),
(13, 1, '2021-04-22 15:59:06', '2021-04-22 09:59:06', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Копирайт', 'copyright', 'publish', 'closed', 'closed', '', 'field_6081489bce81d', '', '', '2021-04-22 15:59:06', '2021-04-22 09:59:06', '', 10, 'http://wt.iilw.ru/?post_type=acf-field&p=13', 2, 'acf-field', '', 0),
(14, 1, '2021-04-22 15:59:06', '2021-04-22 09:59:06', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Контакты', 'kontakty', 'publish', 'closed', 'closed', '', 'field_608148adce81e', '', '', '2021-04-22 16:00:44', '2021-04-22 10:00:44', '', 10, 'http://wt.iilw.ru/?post_type=acf-field&#038;p=14', 4, 'acf-field', '', 0),
(15, 1, '2021-04-22 15:59:06', '2021-04-22 09:59:06', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"table\";s:12:\"button_label\";s:32:\"Добавить соц сеть\";}', 'Соц сети', 'social_links', 'publish', 'closed', 'closed', '', 'field_608148bece81f', '', '', '2021-04-22 16:00:44', '2021-04-22 10:00:44', '', 10, 'http://wt.iilw.ru/?post_type=acf-field&#038;p=15', 5, 'acf-field', '', 0),
(16, 1, '2021-04-22 15:59:06', '2021-04-22 09:59:06', 'a:7:{s:4:\"type\";s:3:\"url\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";}', 'Ссылка', 'link', 'publish', 'closed', 'closed', '', 'field_608148d1ce820', '', '', '2021-04-22 15:59:06', '2021-04-22 09:59:06', '', 15, 'http://wt.iilw.ru/?post_type=acf-field&p=16', 0, 'acf-field', '', 0),
(17, 1, '2021-04-22 16:00:44', '2021-04-22 10:00:44', 'a:11:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";s:0:\"\";s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:0;s:13:\"return_format\";s:2:\"id\";s:2:\"ui\";i:1;}', 'Политика конфиденциальности', 'policy', 'publish', 'closed', 'closed', '', 'field_60814928b086c', '', '', '2021-04-22 16:00:44', '2021-04-22 10:00:44', '', 10, 'http://wt.iilw.ru/?post_type=acf-field&p=17', 3, 'acf-field', '', 0),
(18, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"page_template\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:29:\"page-templates/front-page.php\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Для главной страницы', 'dlya-glavnoj-straniczy', 'publish', 'closed', 'closed', '', 'group_60814a5986bf6', '', '', '2021-04-22 16:52:03', '2021-04-22 10:52:03', '', 0, 'http://wt.iilw.ru/?post_type=acf-field-group&#038;p=18', 0, 'acf-field-group', '', 0),
(19, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:4:\"left\";s:8:\"endpoint\";i:0;}', 'Слайдер', 'slajder', 'publish', 'closed', 'closed', '', 'field_60814a668e1f1', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&p=19', 0, 'acf-field', '', 0),
(20, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:10:{s:4:\"type\";s:8:\"repeater\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"collapsed\";s:0:\"\";s:3:\"min\";s:0:\"\";s:3:\"max\";s:0:\"\";s:6:\"layout\";s:5:\"block\";s:12:\"button_label\";s:27:\"Добавить слайд\";}', 'Слайдер', 'slider', 'publish', 'closed', 'closed', '', 'field_60814a6e8e1f2', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&p=20', 1, 'acf-field', '', 0),
(21, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Заголовок', 'title', 'publish', 'closed', 'closed', '', 'field_60814a958e1f3', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 20, 'http://wt.iilw.ru/?post_type=acf-field&p=21', 0, 'acf-field', '', 0),
(22, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:0:\"\";}', 'Описание', 'desc', 'publish', 'closed', 'closed', '', 'field_60814aa78e1f4', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 20, 'http://wt.iilw.ru/?post_type=acf-field&p=22', 1, 'acf-field', '', 0),
(23, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Фоновое изображение', 'bg', 'publish', 'closed', 'closed', '', 'field_60814ab28e1f5', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 20, 'http://wt.iilw.ru/?post_type=acf-field&p=23', 2, 'acf-field', '', 0),
(24, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Ссылка кнопки', 'btn_link', 'publish', 'closed', 'closed', '', 'field_60814ad58e1f6', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 20, 'http://wt.iilw.ru/?post_type=acf-field&p=24', 3, 'acf-field', '', 0),
(25, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Текст кнопки', 'btn_text', 'publish', 'closed', 'closed', '', 'field_60814ae18e1f7', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 20, 'http://wt.iilw.ru/?post_type=acf-field&p=25', 4, 'acf-field', '', 0),
(26, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'О нас', '', 'publish', 'closed', 'closed', '', 'field_60814afd8e1f8', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&p=26', 2, 'acf-field', '', 0),
(27, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:10:{s:4:\"type\";s:8:\"textarea\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";s:4:\"rows\";s:0:\"\";s:9:\"new_lines\";s:2:\"br\";}', 'Заголовок', 'about_us_title', 'publish', 'closed', 'closed', '', 'field_60814b3c8e1f9', '', '', '2021-04-22 16:29:49', '2021-04-22 10:29:49', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&#038;p=27', 3, 'acf-field', '', 0),
(28, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:10:{s:4:\"type\";s:7:\"wysiwyg\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:4:\"tabs\";s:3:\"all\";s:7:\"toolbar\";s:4:\"full\";s:12:\"media_upload\";i:1;s:5:\"delay\";i:0;}', 'Описание', 'about_us_desc', 'publish', 'closed', 'closed', '', 'field_60814b538e1fa', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&p=28', 4, 'acf-field', '', 0),
(29, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:5:\"array\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Изображение', 'about_us_img', 'publish', 'closed', 'closed', '', 'field_60814b6e8e1fb', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&p=29', 5, 'acf-field', '', 0),
(30, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Программы', 'programmy', 'publish', 'closed', 'closed', '', 'field_60814b938e1fc', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&p=30', 6, 'acf-field', '', 0),
(31, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Заголовок', 'programms_title', 'publish', 'closed', 'closed', '', 'field_60814b9a8e1fd', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&p=31', 7, 'acf-field', '', 0),
(32, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:11:{s:4:\"type\";s:11:\"post_object\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"post_type\";a:1:{i:0;s:14:\"post_programms\";}s:8:\"taxonomy\";s:0:\"\";s:10:\"allow_null\";i:0;s:8:\"multiple\";i:1;s:13:\"return_format\";s:6:\"object\";s:2:\"ui\";i:1;}', 'Список программ', 'programms_items', 'publish', 'closed', 'closed', '', 'field_60814bb08e1fe', '', '', '2021-04-22 16:52:03', '2021-04-22 10:52:03', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&#038;p=32', 8, 'acf-field', '', 0),
(33, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:7:{s:4:\"type\";s:3:\"tab\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:9:\"placement\";s:3:\"top\";s:8:\"endpoint\";i:0;}', 'Новости', 'novosti', 'publish', 'closed', 'closed', '', 'field_60814bed8e1ff', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&p=33', 9, 'acf-field', '', 0),
(34, 1, '2021-04-22 16:12:27', '2021-04-22 10:12:27', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Заголовок', 'news_title', 'publish', 'closed', 'closed', '', 'field_60814bf98e200', '', '', '2021-04-22 16:12:27', '2021-04-22 10:12:27', '', 18, 'http://wt.iilw.ru/?post_type=acf-field&p=34', 10, 'acf-field', '', 0),
(36, 1, '2021-04-22 16:14:54', '2021-04-22 10:14:54', '<!-- wp:paragraph -->\n<p>Страница в разработке</p>\n<!-- /wp:paragraph -->', 'О нас', '', 'publish', 'closed', 'closed', '', 'o-nas', '', '', '2021-04-23 00:12:01', '2021-04-22 18:12:01', '', 0, 'http://wt.iilw.ru/?page_id=36', 0, 'page', '', 0),
(37, 1, '2021-04-22 16:14:54', '2021-04-22 10:14:54', '', 'О нас', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2021-04-22 16:14:54', '2021-04-22 10:14:54', '', 36, 'http://wt.iilw.ru/?p=37', 0, 'revision', '', 0),
(38, 1, '2021-04-22 16:15:09', '2021-04-22 10:15:09', '<!-- wp:paragraph -->\n<p>Страница в разработке</p>\n<!-- /wp:paragraph -->', 'Программы', '', 'publish', 'closed', 'closed', '', 'programms', '', '', '2021-04-23 00:12:17', '2021-04-22 18:12:17', '', 0, 'http://wt.iilw.ru/?page_id=38', 0, 'page', '', 0),
(39, 1, '2021-04-22 16:15:09', '2021-04-22 10:15:09', '', 'Программы', '', 'inherit', 'closed', 'closed', '', '38-revision-v1', '', '', '2021-04-22 16:15:09', '2021-04-22 10:15:09', '', 38, 'http://wt.iilw.ru/?p=39', 0, 'revision', '', 0),
(40, 1, '2021-04-22 16:15:20', '2021-04-22 10:15:20', '<!-- wp:paragraph -->\n<p>Страница в разработке</p>\n<!-- /wp:paragraph -->', 'Пресс-центр', '', 'publish', 'closed', 'closed', '', 'news', '', '', '2021-04-23 00:12:10', '2021-04-22 18:12:10', '', 0, 'http://wt.iilw.ru/?page_id=40', 0, 'page', '', 0),
(41, 1, '2021-04-22 16:15:20', '2021-04-22 10:15:20', '', 'Пресс-центр', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2021-04-22 16:15:20', '2021-04-22 10:15:20', '', 40, 'http://wt.iilw.ru/?p=41', 0, 'revision', '', 0),
(42, 1, '2021-04-22 16:15:28', '2021-04-22 10:15:28', '<!-- wp:paragraph -->\n<p>Страница в разработке</p>\n<!-- /wp:paragraph -->', 'Контакты', '', 'publish', 'closed', 'closed', '', 'kontakty', '', '', '2021-04-23 00:11:54', '2021-04-22 18:11:54', '', 0, 'http://wt.iilw.ru/?page_id=42', 0, 'page', '', 0),
(43, 1, '2021-04-22 16:15:28', '2021-04-22 10:15:28', '', 'Контакты', '', 'inherit', 'closed', 'closed', '', '42-revision-v1', '', '', '2021-04-22 16:15:28', '2021-04-22 10:15:28', '', 42, 'http://wt.iilw.ru/?p=43', 0, 'revision', '', 0),
(44, 1, '2021-04-22 16:18:28', '2021-04-22 10:15:48', ' ', '', '', 'publish', 'closed', 'closed', '', '44', '', '', '2021-04-22 16:18:28', '2021-04-22 10:18:28', '', 0, 'http://wt.iilw.ru/?p=44', 4, 'nav_menu_item', '', 0),
(45, 1, '2021-04-22 16:18:28', '2021-04-22 10:15:48', ' ', '', '', 'publish', 'closed', 'closed', '', '45', '', '', '2021-04-22 16:18:28', '2021-04-22 10:18:28', '', 0, 'http://wt.iilw.ru/?p=45', 3, 'nav_menu_item', '', 0),
(46, 1, '2021-04-22 16:18:28', '2021-04-22 10:15:48', ' ', '', '', 'publish', 'closed', 'closed', '', '46', '', '', '2021-04-22 16:18:28', '2021-04-22 10:18:28', '', 0, 'http://wt.iilw.ru/?p=46', 2, 'nav_menu_item', '', 0),
(47, 1, '2021-04-22 16:18:28', '2021-04-22 10:15:48', ' ', '', '', 'publish', 'closed', 'closed', '', '47', '', '', '2021-04-22 16:18:28', '2021-04-22 10:18:28', '', 0, 'http://wt.iilw.ru/?p=47', 1, 'nav_menu_item', '', 0),
(48, 1, '2021-04-22 16:27:53', '2021-04-22 10:27:53', 'project_bf@mail.ru\nroot', 'project_bf@mail.ru', '', 'publish', 'closed', 'closed', '', 'project_bf-mail-ru', '', '', '2021-04-22 16:27:53', '2021-04-22 10:27:53', '', 0, 'http://wt.iilw.ru/?p=48', 0, 'flamingo_contact', '', 0),
(49, 1, '2021-04-22 16:28:07', '2021-04-22 10:28:07', '', 'logo', '', 'inherit', '', 'closed', '', 'logo', '', '', '2021-04-22 16:28:07', '2021-04-22 10:28:07', '', 0, 'http://wt.iilw.ru/wp-content/uploads/2021/04/logo.svg', 0, 'attachment', 'image/svg+xml', 0),
(50, 1, '2021-04-22 16:28:55', '2021-04-22 10:28:55', '', 'slide-bg', '', 'inherit', '', 'closed', '', 'slide-bg', '', '', '2021-04-22 16:28:55', '2021-04-22 10:28:55', '', 7, 'http://wt.iilw.ru/wp-content/uploads/2021/04/slide-bg.jpg', 0, 'attachment', 'image/jpeg', 0),
(51, 1, '2021-04-22 16:29:51', '2021-04-22 10:29:51', '', 'Главная страница', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-04-22 16:29:51', '2021-04-22 10:29:51', '', 7, 'http://wt.iilw.ru/?p=51', 0, 'revision', '', 0),
(52, 1, '2021-04-22 16:30:41', '2021-04-22 10:30:41', '', 'about-us-img', '', 'inherit', '', 'closed', '', 'about-us-img', '', '', '2021-04-22 16:30:41', '2021-04-22 10:30:41', '', 7, 'http://wt.iilw.ru/wp-content/uploads/2021/04/about-us-img.jpg', 0, 'attachment', 'image/jpeg', 0),
(53, 1, '2021-04-22 16:30:46', '2021-04-22 10:30:46', '', 'Главная страница', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-04-22 16:30:46', '2021-04-22 10:30:46', '', 7, 'http://wt.iilw.ru/?p=53', 0, 'revision', '', 0),
(54, 1, '2021-04-22 16:43:21', '0000-00-00 00:00:00', '', 'Черновик', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2021-04-22 16:43:21', '0000-00-00 00:00:00', '', 0, 'http://wt.iilw.ru/?post_type=post_programms&p=54', 0, 'post_programms', '', 0),
(55, 1, '2021-04-22 16:44:58', '2021-04-22 10:44:58', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:9:\"post_type\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:14:\"post_programms\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Для программ', 'dlya-programm', 'publish', 'closed', 'closed', '', 'group_6081535aa68dd', '', '', '2021-04-22 16:44:58', '2021-04-22 10:44:58', '', 0, 'http://wt.iilw.ru/?post_type=acf-field-group&#038;p=55', 0, 'acf-field-group', '', 0),
(56, 1, '2021-04-22 16:44:58', '2021-04-22 10:44:58', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Изображение', 'img', 'publish', 'closed', 'closed', '', 'field_6081539e7eacf', '', '', '2021-04-22 16:44:58', '2021-04-22 10:44:58', '', 55, 'http://wt.iilw.ru/?post_type=acf-field&p=56', 0, 'acf-field', '', 0),
(57, 1, '2021-04-22 16:46:43', '2021-04-22 10:46:43', '', '“Мы вместе”', '', 'publish', 'closed', 'closed', '', 'my-vmeste', '', '', '2021-04-22 16:46:43', '2021-04-22 10:46:43', '', 0, 'http://wt.iilw.ru/?post_type=post_programms&#038;p=57', 0, 'post_programms', '', 0),
(58, 1, '2021-04-22 16:46:38', '2021-04-22 10:46:38', '', 'programm-img1', '', 'inherit', '', 'closed', '', 'programm-img1', '', '', '2021-04-22 16:46:38', '2021-04-22 10:46:38', '', 57, 'http://wt.iilw.ru/wp-content/uploads/2021/04/programm-img1.jpg', 0, 'attachment', 'image/jpeg', 0),
(59, 1, '2021-04-22 16:47:25', '2021-04-22 10:47:25', '', '“День чистоты”', '', 'publish', 'closed', 'closed', '', 'den-chistoty', '', '', '2021-04-22 16:47:25', '2021-04-22 10:47:25', '', 0, 'http://wt.iilw.ru/?post_type=post_programms&#038;p=59', 0, 'post_programms', '', 0),
(60, 1, '2021-04-22 16:47:20', '2021-04-22 10:47:20', '', 'programm-img2', '', 'inherit', '', 'closed', '', 'programm-img2', '', '', '2021-04-22 16:47:20', '2021-04-22 10:47:20', '', 59, 'http://wt.iilw.ru/wp-content/uploads/2021/04/programm-img2.jpg', 0, 'attachment', 'image/jpeg', 0),
(61, 1, '2021-04-22 16:48:01', '2021-04-22 10:48:01', '', '“Город и село: мы вместе”', '', 'publish', 'closed', 'closed', '', 'gorod-i-selo-my-vmeste', '', '', '2021-04-22 16:48:01', '2021-04-22 10:48:01', '', 0, 'http://wt.iilw.ru/?post_type=post_programms&#038;p=61', 0, 'post_programms', '', 0),
(62, 1, '2021-04-22 16:47:57', '2021-04-22 10:47:57', '', 'programm-img3', '', 'inherit', '', 'closed', '', 'programm-img3', '', '', '2021-04-22 16:47:57', '2021-04-22 10:47:57', '', 61, 'http://wt.iilw.ru/wp-content/uploads/2021/04/programm-img3.jpg', 0, 'attachment', 'image/jpeg', 0),
(63, 1, '2021-04-22 16:48:17', '2021-04-22 10:48:17', '', 'Главная страница', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-04-22 16:48:17', '2021-04-22 10:48:17', '', 7, 'http://wt.iilw.ru/?p=63', 0, 'revision', '', 0),
(64, 1, '2021-04-22 16:48:33', '2021-04-22 10:48:33', '', 'Главная страница', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-04-22 16:48:33', '2021-04-22 10:48:33', '', 7, 'http://wt.iilw.ru/?p=64', 0, 'revision', '', 0),
(65, 1, '2021-04-22 16:48:50', '2021-04-22 10:48:50', '', 'Главная страница', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-04-22 16:48:50', '2021-04-22 10:48:50', '', 7, 'http://wt.iilw.ru/?p=65', 0, 'revision', '', 0),
(66, 1, '2021-04-22 17:34:11', '2021-04-22 11:34:11', '<!-- wp:paragraph -->\n<p>По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...</p>\n<!-- /wp:paragraph -->', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'publish', 'closed', 'open', '', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas', '', '', '2021-04-22 17:34:11', '2021-04-22 11:34:11', '', 0, 'http://wt.iilw.ru/?p=66', 0, 'post', '', 0),
(67, 1, '2021-04-22 17:33:59', '2021-04-22 11:33:59', '', 'news-img', '', 'inherit', '', 'closed', '', 'news-img', '', '', '2021-04-22 17:33:59', '2021-04-22 11:33:59', '', 66, 'http://wt.iilw.ru/wp-content/uploads/2021/04/news-img.jpg', 0, 'attachment', 'image/jpeg', 0),
(68, 1, '2021-04-22 17:34:11', '2021-04-22 11:34:11', '<!-- wp:paragraph -->\n<p>По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...</p>\n<!-- /wp:paragraph -->', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'inherit', 'closed', 'closed', '', '66-revision-v1', '', '', '2021-04-22 17:34:11', '2021-04-22 11:34:11', '', 66, 'http://wt.iilw.ru/?p=68', 0, 'revision', '', 0),
(69, 1, '2021-04-22 17:34:25', '2021-04-22 11:34:25', '<!-- wp:paragraph -->\n<p>По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...</p>\n<!-- /wp:paragraph -->', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'publish', 'closed', 'open', '', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas-2', '', '', '2021-04-22 17:34:25', '2021-04-22 11:34:25', '', 0, 'http://wt.iilw.ru/?p=69', 0, 'post', '', 0),
(70, 1, '2021-04-22 17:34:25', '2021-04-22 11:34:25', '<!-- wp:paragraph -->\n<p>По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...По своей сути рыбатекст является альтернативой традиционному lorem...</p>\n<!-- /wp:paragraph -->', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'inherit', 'closed', 'closed', '', '69-revision-v1', '', '', '2021-04-22 17:34:25', '2021-04-22 11:34:25', '', 69, 'http://wt.iilw.ru/?p=70', 0, 'revision', '', 0),
(71, 1, '2021-04-22 17:36:04', '2021-04-22 11:36:04', '', 'V Форум «Эндаументы 2021. Больше, чем деньги»', '', 'publish', 'closed', 'open', '', 'v-forum-endaumenty-2021-bolshe-chem-dengi', '', '', '2021-04-22 17:36:20', '2021-04-22 11:36:20', '', 0, 'http://wt.iilw.ru/?p=71', 0, 'post', '', 0),
(72, 1, '2021-04-22 17:36:01', '2021-04-22 11:36:01', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"post_category\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:15:\"category:anonsy\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Для новостей', 'dlya-novostej', 'publish', 'closed', 'closed', '', 'group_60815f5dd7dfb', '', '', '2021-04-22 17:36:01', '2021-04-22 11:36:01', '', 0, 'http://wt.iilw.ru/?post_type=acf-field-group&#038;p=72', 0, 'acf-field-group', '', 0),
(73, 1, '2021-04-22 17:36:01', '2021-04-22 11:36:01', 'a:7:{s:4:\"type\";s:11:\"time_picker\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:14:\"display_format\";s:3:\"H:i\";s:13:\"return_format\";s:3:\"H:i\";}', 'Время проведения мероприятия', 'news_time', 'publish', 'closed', 'closed', '', 'field_60815f628044b', '', '', '2021-04-22 17:36:01', '2021-04-22 11:36:01', '', 72, 'http://wt.iilw.ru/?post_type=acf-field&p=73', 0, 'acf-field', '', 0),
(74, 1, '2021-04-22 17:36:04', '2021-04-22 11:36:04', '', 'V Форум «Эндаументы 2021. Больше, чем деньги»', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2021-04-22 17:36:04', '2021-04-22 11:36:04', '', 71, 'http://wt.iilw.ru/?p=74', 0, 'revision', '', 0),
(75, 1, '2021-04-22 17:36:20', '2021-04-22 11:36:20', '', 'V Форум «Эндаументы 2021. Больше, чем деньги»', '', 'inherit', 'closed', 'closed', '', '71-revision-v1', '', '', '2021-04-22 17:36:20', '2021-04-22 11:36:20', '', 71, 'http://wt.iilw.ru/?p=75', 0, 'revision', '', 0),
(76, 1, '2021-04-22 17:36:23', '2021-04-22 11:36:23', '', 'V Форум «Эндаументы 2021. Больше, чем деньги»', '', 'publish', 'closed', 'open', '', 'v-forum-endaumenty-2021-bolshe-chem-dengi-2', '', '', '2021-04-22 17:36:30', '2021-04-22 11:36:30', '', 0, 'http://wt.iilw.ru/?p=76', 0, 'post', '', 0),
(77, 1, '2021-04-22 17:36:30', '2021-04-22 11:36:30', '', 'V Форум «Эндаументы 2021. Больше, чем деньги»', '', 'inherit', 'closed', 'closed', '', '76-revision-v1', '', '', '2021-04-22 17:36:30', '2021-04-22 11:36:30', '', 76, 'http://wt.iilw.ru/?p=77', 0, 'revision', '', 0),
(78, 1, '2021-04-22 17:38:22', '2021-04-22 11:38:22', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'publish', 'closed', 'open', '', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas-3', '', '', '2021-04-22 17:38:29', '2021-04-22 11:38:29', '', 0, 'http://wt.iilw.ru/?p=78', 0, 'post', '', 0),
(79, 1, '2021-04-22 17:38:15', '2021-04-22 11:38:15', 'a:7:{s:8:\"location\";a:1:{i:0;a:1:{i:0;a:3:{s:5:\"param\";s:13:\"post_category\";s:8:\"operator\";s:2:\"==\";s:5:\"value\";s:17:\"category:my-v-smi\";}}}s:8:\"position\";s:6:\"normal\";s:5:\"style\";s:7:\"default\";s:15:\"label_placement\";s:3:\"top\";s:21:\"instruction_placement\";s:5:\"label\";s:14:\"hide_on_screen\";s:0:\"\";s:11:\"description\";s:0:\"\";}', 'Для новостей(СМИ)', 'dlya-novostejsmi', 'publish', 'closed', 'closed', '', 'group_60815fd9abb22', '', '', '2021-04-22 17:38:15', '2021-04-22 11:38:15', '', 0, 'http://wt.iilw.ru/?post_type=acf-field-group&#038;p=79', 0, 'acf-field-group', '', 0),
(80, 1, '2021-04-22 17:38:15', '2021-04-22 11:38:15', 'a:10:{s:4:\"type\";s:4:\"text\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"default_value\";s:0:\"\";s:11:\"placeholder\";s:0:\"\";s:7:\"prepend\";s:0:\"\";s:6:\"append\";s:0:\"\";s:9:\"maxlength\";s:0:\"\";}', 'Источник новости', 'news_source', 'publish', 'closed', 'closed', '', 'field_60815feacad5a', '', '', '2021-04-22 17:38:15', '2021-04-22 11:38:15', '', 79, 'http://wt.iilw.ru/?post_type=acf-field&p=80', 0, 'acf-field', '', 0),
(81, 1, '2021-04-22 17:38:22', '2021-04-22 11:38:22', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'inherit', 'closed', 'closed', '', '78-revision-v1', '', '', '2021-04-22 17:38:22', '2021-04-22 11:38:22', '', 78, 'http://wt.iilw.ru/?p=81', 0, 'revision', '', 0),
(82, 1, '2021-04-22 17:38:29', '2021-04-22 11:38:29', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'inherit', 'closed', 'closed', '', '78-revision-v1', '', '', '2021-04-22 17:38:29', '2021-04-22 11:38:29', '', 78, 'http://wt.iilw.ru/?p=82', 0, 'revision', '', 0),
(83, 1, '2021-04-22 17:38:35', '2021-04-22 11:38:35', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'publish', 'closed', 'open', '', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas-3-5', '', '', '2021-04-22 17:39:05', '2021-04-22 11:39:05', '', 0, 'http://wt.iilw.ru/?p=83', 0, 'post', '', 0),
(84, 1, '2021-04-22 17:38:37', '2021-04-22 11:38:37', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'publish', 'closed', 'open', '', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas-3-4', '', '', '2021-04-22 17:38:55', '2021-04-22 11:38:55', '', 0, 'http://wt.iilw.ru/?p=84', 0, 'post', '', 0),
(85, 1, '2021-04-22 17:38:40', '2021-04-22 11:38:40', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'publish', 'closed', 'open', '', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas-3-3', '', '', '2021-04-22 17:38:50', '2021-04-22 11:38:50', '', 0, 'http://wt.iilw.ru/?p=85', 0, 'post', '', 0),
(86, 1, '2021-04-22 17:38:42', '2021-04-22 11:38:42', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'publish', 'closed', 'open', '', 'nikolaj-biryukov-davajte-nachnem-dejstvovat-pryamo-sejchas-3-2', '', '', '2021-04-22 17:51:34', '2021-04-22 11:51:34', '', 0, 'http://wt.iilw.ru/?p=86', 0, 'post', '', 0),
(87, 1, '2021-04-22 17:38:47', '2021-04-22 11:38:47', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2021-04-22 17:38:47', '2021-04-22 11:38:47', '', 86, 'http://wt.iilw.ru/?p=87', 0, 'revision', '', 0),
(88, 1, '2021-04-22 17:38:50', '2021-04-22 11:38:50', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'inherit', 'closed', 'closed', '', '85-revision-v1', '', '', '2021-04-22 17:38:50', '2021-04-22 11:38:50', '', 85, 'http://wt.iilw.ru/?p=88', 0, 'revision', '', 0),
(89, 1, '2021-04-22 17:38:55', '2021-04-22 11:38:55', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'inherit', 'closed', 'closed', '', '84-revision-v1', '', '', '2021-04-22 17:38:55', '2021-04-22 11:38:55', '', 84, 'http://wt.iilw.ru/?p=89', 0, 'revision', '', 0),
(90, 1, '2021-04-22 17:39:05', '2021-04-22 11:39:05', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'inherit', 'closed', 'closed', '', '83-revision-v1', '', '', '2021-04-22 17:39:05', '2021-04-22 11:39:05', '', 83, 'http://wt.iilw.ru/?p=90', 0, 'revision', '', 0),
(91, 1, '2021-04-22 17:48:44', '2021-04-22 11:48:44', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»asdasdasda', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2021-04-22 17:48:44', '2021-04-22 11:48:44', '', 86, 'http://wt.iilw.ru/?p=91', 0, 'revision', '', 0),
(92, 1, '2021-04-22 17:51:32', '2021-04-22 11:51:32', '', 'Николай БИРЮКОВ: «Давайте начнем действовать прямо сейчас»', '', 'inherit', 'closed', 'closed', '', '86-revision-v1', '', '', '2021-04-22 17:51:32', '2021-04-22 11:51:32', '', 86, 'http://wt.iilw.ru/?p=92', 0, 'revision', '', 0),
(93, 1, '2021-04-22 18:15:36', '2021-04-22 12:15:36', 'a:15:{s:4:\"type\";s:5:\"image\";s:12:\"instructions\";s:0:\"\";s:8:\"required\";i:0;s:17:\"conditional_logic\";i:0;s:7:\"wrapper\";a:3:{s:5:\"width\";s:0:\"\";s:5:\"class\";s:0:\"\";s:2:\"id\";s:0:\"\";}s:13:\"return_format\";s:3:\"url\";s:12:\"preview_size\";s:6:\"medium\";s:7:\"library\";s:3:\"all\";s:9:\"min_width\";s:0:\"\";s:10:\"min_height\";s:0:\"\";s:8:\"min_size\";s:0:\"\";s:9:\"max_width\";s:0:\"\";s:10:\"max_height\";s:0:\"\";s:8:\"max_size\";s:0:\"\";s:10:\"mime_types\";s:0:\"\";}', 'Иконка', 'icon', 'publish', 'closed', 'closed', '', 'field_608168d9cb5ef', '', '', '2021-04-22 18:15:36', '2021-04-22 12:15:36', '', 15, 'http://wt.iilw.ru/?post_type=acf-field&p=93', 1, 'acf-field', '', 0),
(94, 1, '2021-04-22 18:22:39', '2021-04-22 12:22:39', '', 'youtube-icon', '', 'inherit', '', 'closed', '', 'youtube-icon', '', '', '2021-04-22 18:22:39', '2021-04-22 12:22:39', '', 0, 'http://wt.iilw.ru/wp-content/uploads/2021/04/youtube-icon.svg', 0, 'attachment', 'image/svg+xml', 0),
(95, 1, '2021-04-22 18:22:52', '2021-04-22 12:22:52', '', 'inst-icon', '', 'inherit', '', 'closed', '', 'inst-icon', '', '', '2021-04-22 18:22:52', '2021-04-22 12:22:52', '', 0, 'http://wt.iilw.ru/wp-content/uploads/2021/04/inst-icon.svg', 0, 'attachment', 'image/svg+xml', 0),
(96, 1, '2021-04-22 18:49:21', '2021-04-22 12:49:21', '', 'arrow-prev', '', 'inherit', '', 'closed', '', 'arrow-prev', '', '', '2021-04-22 18:49:21', '2021-04-22 12:49:21', '', 0, 'http://wt.iilw.ru/wp-content/uploads/2021/04/arrow-prev.svg', 0, 'attachment', 'image/svg+xml', 0),
(97, 1, '2021-04-22 18:49:21', '2021-04-22 12:49:21', '', 'arrow-next', '', 'inherit', '', 'closed', '', 'arrow-next', '', '', '2021-04-22 18:49:21', '2021-04-22 12:49:21', '', 0, 'http://wt.iilw.ru/wp-content/uploads/2021/04/arrow-next.svg', 0, 'attachment', 'image/svg+xml', 0),
(98, 1, '2021-04-22 19:06:08', '2021-04-22 13:06:08', '', 'Главная страница', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-04-22 19:06:08', '2021-04-22 13:06:08', '', 7, 'http://wt.iilw.ru/?p=98', 0, 'revision', '', 0),
(99, 1, '2021-04-22 19:07:07', '2021-04-22 13:07:07', '', 'Главная страница', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-04-22 19:07:07', '2021-04-22 13:07:07', '', 7, 'http://wt.iilw.ru/?p=99', 0, 'revision', '', 0),
(100, 1, '2021-04-22 20:11:47', '2021-04-22 14:11:47', '', 'Главная страница', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-04-22 20:11:47', '2021-04-22 14:11:47', '', 7, 'http://wt.iilw.ru/?p=100', 0, 'revision', '', 0),
(101, 1, '2021-04-23 00:11:54', '2021-04-22 18:11:54', '<!-- wp:paragraph -->\n<p>Страница в разработке</p>\n<!-- /wp:paragraph -->', 'Контакты', '', 'inherit', 'closed', 'closed', '', '42-revision-v1', '', '', '2021-04-23 00:11:54', '2021-04-22 18:11:54', '', 42, 'http://wt.iilw.ru/?p=101', 0, 'revision', '', 0),
(102, 1, '2021-04-23 00:12:01', '2021-04-22 18:12:01', '<!-- wp:paragraph -->\n<p>Страница в разработке</p>\n<!-- /wp:paragraph -->', 'О нас', '', 'inherit', 'closed', 'closed', '', '36-revision-v1', '', '', '2021-04-23 00:12:01', '2021-04-22 18:12:01', '', 36, 'http://wt.iilw.ru/?p=102', 0, 'revision', '', 0),
(103, 1, '2021-04-23 00:12:10', '2021-04-22 18:12:10', '<!-- wp:paragraph -->\n<p>Страница в разработке</p>\n<!-- /wp:paragraph -->', 'Пресс-центр', '', 'inherit', 'closed', 'closed', '', '40-revision-v1', '', '', '2021-04-23 00:12:10', '2021-04-22 18:12:10', '', 40, 'http://wt.iilw.ru/?p=103', 0, 'revision', '', 0),
(104, 1, '2021-04-23 00:12:17', '2021-04-22 18:12:17', '<!-- wp:paragraph -->\n<p>Страница в разработке</p>\n<!-- /wp:paragraph -->', 'Программы', '', 'inherit', 'closed', 'closed', '', '38-revision-v1', '', '', '2021-04-23 00:12:17', '2021-04-22 18:12:17', '', 38, 'http://wt.iilw.ru/?p=104', 0, 'revision', '', 0),
(105, 1, '2021-04-23 01:47:24', '2021-04-22 19:47:24', '', 'Главная страница', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2021-04-23 01:47:24', '2021-04-22 19:47:24', '', 7, 'http://wt.iilw.ru/?p=105', 0, 'revision', '', 0),
(106, 1, '2021-04-23 12:31:40', '2021-04-23 06:31:40', '', 'favicon', '', 'inherit', '', 'closed', '', 'favicon', '', '', '2021-04-23 12:31:40', '2021-04-23 06:31:40', '', 0, 'http://wt.iilw.ru/wp-content/uploads/2021/04/favicon.png', 0, 'attachment', 'image/png', 0),
(107, 1, '2021-04-23 12:31:47', '2021-04-23 06:31:47', 'http://wt.iilw.ru/wp-content/uploads/2021/04/cropped-favicon.png', 'cropped-favicon.png', '', 'inherit', '', 'closed', '', 'cropped-favicon-png', '', '', '2021-04-23 12:31:47', '2021-04-23 06:31:47', '', 0, 'http://wt.iilw.ru/wp-content/uploads/2021/04/cropped-favicon.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `wp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(108, 1, '2021-04-23 12:31:51', '2021-04-23 06:31:51', '{\n    \"site_icon\": {\n        \"value\": 107,\n        \"type\": \"option\",\n        \"user_id\": 1,\n        \"date_modified_gmt\": \"2021-04-23 06:31:51\"\n    }\n}', '', '', 'trash', 'closed', 'closed', '', '530b1ef0-f244-42a9-a3dd-fc6ab8e5ae64', '', '', '2021-04-23 12:31:51', '2021-04-23 06:31:51', '', 0, 'http://wt.iilw.ru/530b1ef0-f244-42a9-a3dd-fc6ab8e5ae64/', 0, 'customize_changeset', '', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_smush_dir_images`
--

CREATE TABLE `wp_smush_dir_images` (
  `id` mediumint(9) NOT NULL,
  `path` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `path_hash` char(32) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `resize` varchar(55) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `lossy` varchar(55) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `error` varchar(55) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `image_size` int(10) UNSIGNED DEFAULT NULL,
  `orig_size` int(10) UNSIGNED DEFAULT NULL,
  `file_time` int(10) UNSIGNED DEFAULT NULL,
  `last_scan` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `meta` text COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_termmeta`
--

CREATE TABLE `wp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Структура таблицы `wp_terms`
--

CREATE TABLE `wp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_terms`
--

INSERT INTO `wp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Без рубрики', 'bez-rubriki', 0),
(2, 'Меню', 'menyu', 0),
(3, 'Омский региональный конкурс-фестиваль детского и юношеского творчества', 'omskij-regionalnyj-konkurs-festival-detskogo-i-yunosheskogo-tvorchestva', 0),
(4, 'Эко-проект', 'eko-proekt', 0),
(5, 'Программа', 'programma', 0),
(6, 'Новости', 'novosti', 0),
(7, 'Мы в СМИ', 'my-v-smi', 0),
(8, 'Анонсы', 'anonsy', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_term_relationships`
--

CREATE TABLE `wp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_term_relationships`
--

INSERT INTO `wp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(44, 2, 0),
(45, 2, 0),
(46, 2, 0),
(47, 2, 0),
(57, 3, 0),
(59, 4, 0),
(61, 5, 0),
(66, 6, 0),
(69, 6, 0),
(71, 8, 0),
(76, 8, 0),
(78, 7, 0),
(83, 7, 0),
(84, 7, 0),
(85, 7, 0),
(86, 7, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_term_taxonomy`
--

CREATE TABLE `wp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_term_taxonomy`
--

INSERT INTO `wp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 0),
(2, 2, 'nav_menu', '', 0, 4),
(3, 3, 'programms_cat', '', 0, 1),
(4, 4, 'programms_cat', '', 0, 1),
(5, 5, 'programms_cat', '', 0, 1),
(6, 6, 'category', '', 0, 2),
(7, 7, 'category', '', 0, 5),
(8, 8, 'category', '', 0, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `wp_usermeta`
--

CREATE TABLE `wp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_usermeta`
--

INSERT INTO `wp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'root'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'wp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'wp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '1'),
(16, 1, 'session_tokens', 'a:3:{s:64:\"a53b45b50a3c397a46b52db35ffd78e75f7d228df9cac6816c91735f032ad45e\";a:4:{s:10:\"expiration\";i:1619257141;s:2:\"ip\";s:12:\"176.62.84.87\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/89.0.4389.128 Safari/537.36\";s:5:\"login\";i:1619084341;}s:64:\"37f4f2c482fefbc63cab9f1a3e476f843aeceb8fc481e9c17bdd25bf4623ac03\";a:4:{s:10:\"expiration\";i:1619287586;s:2:\"ip\";s:13:\"217.25.223.95\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36\";s:5:\"login\";i:1619114786;}s:64:\"5eb21ce6a877bdba9dd9a2f6daa4477f1e6c16c7e71ec63728993154c1303c5a\";a:4:{s:10:\"expiration\";i:1619329569;s:2:\"ip\";s:12:\"176.62.84.87\";s:2:\"ua\";s:114:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.85 Safari/537.36\";s:5:\"login\";i:1619156769;}}'),
(17, 1, 'wp_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'community-events-location', 'a:1:{s:2:\"ip\";s:11:\"176.62.84.0\";}'),
(19, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(20, 1, 'metaboxhidden_nav-menus', 'a:2:{i:0;s:28:\"add-post-type-post_programms\";i:1;s:12:\"add-post_tag\";}'),
(21, 1, 'wp_user-settings', 'libraryContent=browse&editor=tinymce'),
(22, 1, 'wp_user-settings-time', '1619087442');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_users`
--

CREATE TABLE `wp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_users`
--

INSERT INTO `wp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'root', '$P$BCGQtiteWc.fpSfhBm9U1Y/pyaBhjn0', 'root', 'project_bf@mail.ru', 'http://wt.iilw.ru', '2021-04-22 09:38:51', '', 0, 'root');

-- --------------------------------------------------------

--
-- Структура таблицы `wp_wpmailsmtp_tasks_meta`
--

CREATE TABLE `wp_wpmailsmtp_tasks_meta` (
  `id` bigint(20) NOT NULL,
  `action` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `data` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Дамп данных таблицы `wp_wpmailsmtp_tasks_meta`
--

INSERT INTO `wp_wpmailsmtp_tasks_meta` (`id`, `action`, `data`, `date`) VALUES
(1, 'wp_mail_smtp_admin_notifications_update', 'W10=', '2021-04-22 10:29:49');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  ADD PRIMARY KEY (`action_id`),
  ADD KEY `hook` (`hook`),
  ADD KEY `status` (`status`),
  ADD KEY `scheduled_date_gmt` (`scheduled_date_gmt`),
  ADD KEY `args` (`args`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `last_attempt_gmt` (`last_attempt_gmt`),
  ADD KEY `claim_id` (`claim_id`);

--
-- Индексы таблицы `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  ADD PRIMARY KEY (`claim_id`),
  ADD KEY `date_created_gmt` (`date_created_gmt`);

--
-- Индексы таблицы `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  ADD PRIMARY KEY (`group_id`),
  ADD KEY `slug` (`slug`(191));

--
-- Индексы таблицы `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  ADD PRIMARY KEY (`log_id`),
  ADD KEY `action_id` (`action_id`),
  ADD KEY `log_date_gmt` (`log_date_gmt`);

--
-- Индексы таблицы `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `comment_id` (`comment_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_comments`
--
ALTER TABLE `wp_comments`
  ADD PRIMARY KEY (`comment_ID`),
  ADD KEY `comment_post_ID` (`comment_post_ID`),
  ADD KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  ADD KEY `comment_date_gmt` (`comment_date_gmt`),
  ADD KEY `comment_parent` (`comment_parent`),
  ADD KEY `comment_author_email` (`comment_author_email`(10));

--
-- Индексы таблицы `wp_links`
--
ALTER TABLE `wp_links`
  ADD PRIMARY KEY (`link_id`),
  ADD KEY `link_visible` (`link_visible`);

--
-- Индексы таблицы `wp_options`
--
ALTER TABLE `wp_options`
  ADD PRIMARY KEY (`option_id`),
  ADD UNIQUE KEY `option_name` (`option_name`),
  ADD KEY `autoload` (`autoload`);

--
-- Индексы таблицы `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `post_id` (`post_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_posts`
--
ALTER TABLE `wp_posts`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `post_name` (`post_name`(191)),
  ADD KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  ADD KEY `post_parent` (`post_parent`),
  ADD KEY `post_author` (`post_author`);

--
-- Индексы таблицы `wp_smush_dir_images`
--
ALTER TABLE `wp_smush_dir_images`
  ADD UNIQUE KEY `id` (`id`),
  ADD UNIQUE KEY `path_hash` (`path_hash`),
  ADD KEY `image_size` (`image_size`);

--
-- Индексы таблицы `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  ADD PRIMARY KEY (`meta_id`),
  ADD KEY `term_id` (`term_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_terms`
--
ALTER TABLE `wp_terms`
  ADD PRIMARY KEY (`term_id`),
  ADD KEY `slug` (`slug`(191)),
  ADD KEY `name` (`name`(191));

--
-- Индексы таблицы `wp_term_relationships`
--
ALTER TABLE `wp_term_relationships`
  ADD PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  ADD KEY `term_taxonomy_id` (`term_taxonomy_id`);

--
-- Индексы таблицы `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  ADD PRIMARY KEY (`term_taxonomy_id`),
  ADD UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  ADD KEY `taxonomy` (`taxonomy`);

--
-- Индексы таблицы `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  ADD PRIMARY KEY (`umeta_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `meta_key` (`meta_key`(191));

--
-- Индексы таблицы `wp_users`
--
ALTER TABLE `wp_users`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `user_login_key` (`user_login`),
  ADD KEY `user_nicename` (`user_nicename`),
  ADD KEY `user_email` (`user_email`);

--
-- Индексы таблицы `wp_wpmailsmtp_tasks_meta`
--
ALTER TABLE `wp_wpmailsmtp_tasks_meta`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `wp_actionscheduler_actions`
--
ALTER TABLE `wp_actionscheduler_actions`
  MODIFY `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT для таблицы `wp_actionscheduler_claims`
--
ALTER TABLE `wp_actionscheduler_claims`
  MODIFY `claim_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=359;
--
-- AUTO_INCREMENT для таблицы `wp_actionscheduler_groups`
--
ALTER TABLE `wp_actionscheduler_groups`
  MODIFY `group_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `wp_actionscheduler_logs`
--
ALTER TABLE `wp_actionscheduler_logs`
  MODIFY `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `wp_commentmeta`
--
ALTER TABLE `wp_commentmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `wp_comments`
--
ALTER TABLE `wp_comments`
  MODIFY `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `wp_links`
--
ALTER TABLE `wp_links`
  MODIFY `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `wp_options`
--
ALTER TABLE `wp_options`
  MODIFY `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=734;
--
-- AUTO_INCREMENT для таблицы `wp_postmeta`
--
ALTER TABLE `wp_postmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=600;
--
-- AUTO_INCREMENT для таблицы `wp_posts`
--
ALTER TABLE `wp_posts`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=109;
--
-- AUTO_INCREMENT для таблицы `wp_smush_dir_images`
--
ALTER TABLE `wp_smush_dir_images`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `wp_termmeta`
--
ALTER TABLE `wp_termmeta`
  MODIFY `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `wp_terms`
--
ALTER TABLE `wp_terms`
  MODIFY `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `wp_term_taxonomy`
--
ALTER TABLE `wp_term_taxonomy`
  MODIFY `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT для таблицы `wp_usermeta`
--
ALTER TABLE `wp_usermeta`
  MODIFY `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `wp_users`
--
ALTER TABLE `wp_users`
  MODIFY `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `wp_wpmailsmtp_tasks_meta`
--
ALTER TABLE `wp_wpmailsmtp_tasks_meta`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
