<div class="page-header flex aic" style="background: url(<?php the_field('banner_bg', 'option'); ?>) no-repeat 50% 50%/cover;">
	<div class="container">
		<div class="flex aic">
			<h1 class="title"><?php the_title(); ?></h1>
		</div>
	</div>
</div>

<div class="container">
	<?php the_content(); ?>
</div>