<?php if (is_front_page()): ?>
	<div class="about-us-desc flex">
		<?php
			$id = get_field('about_us_page');
			$gallery = get_field('certificates', $id);
			$animation_desc = get_field('guarantee_short_desc', $id);
		?>
		<div class="f50">
			<div>
				<div><?php the_field('short_desc', $id); ?></div>
				<?php if ($gallery): ?>
					<div class="about-us-slider">
						<?php foreach ($gallery as $gal): ?>
							<a href="<?php echo $gal['url']; ?>" data-fancybox="cert-gallery">
								<img src="<?php echo $gal['sizes']['about_us_img']; ?>" alt="<?php echo $gal['title']; ?>">
							</a>
						<?php endforeach ?>
					</div>
				<?php endif ?>
			</div>
		</div>
		<div class="about-us-benefits f50">
			<div>
				<?php if (have_rows('benefits', $id)): ?>
					<?php while (have_rows('benefits', $id)): the_row();
						$desc = get_sub_field('desc');
					?>
						<p><?php echo $desc; ?></p>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<div class="about-us-add-desc fb">
		<div class="flex">
			<p><?php echo $animation_desc; ?></p>
			<p><?php echo $animation_desc; ?></p>
			<p><?php echo $animation_desc; ?></p>
			<p><?php echo $animation_desc; ?></p>
			<p><?php echo $animation_desc; ?></p>
			<p><?php echo $animation_desc; ?></p>
			<p><?php echo $animation_desc; ?></p>
		</div>
	</div>
<?php else: ?>
	<?php
		$gallery = get_field('certificates');
		$img = get_field('img');
		$guarantee_img = get_field('guarantee_img');
	?>
	<div class="about-us-page">
		<div class="about-us-desc flex">
			<div class="f50">
				<div>
					<img src="<?php echo $img['url']; ?>" alt="<?php echo $img['title']; ?>">
				</div>
			</div>
			<div class="f50">
				<div><?php the_field('desc'); ?></div>
			</div>
		</div>
		<div class="about-us-benefits">
			<div class="container">
				<?php if (have_rows('benefits')): ?>
					<?php while (have_rows('benefits')): the_row();
						$desc = get_sub_field('desc');
					?>
						<p><?php echo $desc; ?></p>
					<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
		<div class="about-us-certificates">
			<div class="container">
				<div class="title"><?php the_field('certificates_title'); ?></div>
				<?php if ($gallery): ?>
					<div class="about-us-certificates-slider">
					<?php foreach ($gallery as $gal): ?>
						<a href="<?php echo $gal['url']; ?>" data-fancybox="cert-gallery">
							<img src="<?php echo $gal['sizes']['about_us_page_img']; ?>" alt="<?php echo $gal['title']; ?>">
						</a>
					<?php endforeach ?>
					</div>
				<?php endif ?>
			</div>
		</div>
		<div class="about-us-guarantee">
			<div class="container">
				<div class="flex aic">
					<img src="<?php echo $guarantee_img['url']; ?>" alt="<?php echo $guarantee_img['title']; ?>">
					<div><?php the_field('guarantee_desc'); ?></div>
				</div>
			</div>
		</div>
	</div>
<?php endif ?>