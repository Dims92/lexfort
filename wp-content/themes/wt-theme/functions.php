<?php

add_image_size( 'home_news_img', 192, 142, array( 'center', 'center' ), true );
add_image_size( 'post_news_img', 370, 227, array( 'center', 'center' ), true );
add_image_size( 'about_us_img', 470, 401, array( 'center', 'center' ), true );

function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyC2aZttNBmbsDALKLgexJ39vn4cS2u3t44');
}

add_action('acf/init', 'my_acf_init');

function my_myme_types($mime_types){
		$mime_types['svg'] = 'image/svg+xml';
		return $mime_types;
}
add_filter('upload_mimes', 'my_myme_types', 1, 1);

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title'   => 'Основные настройки темы',
		'menu_title'   => 'Основные настройки темы',
		'menu_slug'   => 'theme-add-settings',
		'capability'   => 'edit_posts',
		'redirect'   => false
	));
}

// register_nav_menus(array(
// 	'footer-menu'    => 'Footer menu',
// ));

/* Новости */

add_action('init', 'post_news');
function post_news(){
	register_post_type('post_news', array(
		'labels'							=> array(
			'name'								=> 'Новости', // Основное название типа записи
			'singular_name'				=> 'Новость', // отдельное название записи типа Book
			'add_new'							=> 'Добавить новую',
			'add_new_item'				=> 'Добавить новую новость',
			'edit_item'						=> 'Редактировать новость',
			'new_item'						=> 'Новая Новость',
			'view_item'						=> 'Посмотреть новость',
			'search_items'				=> 'Найти новость',
			'not_found'						=> 'Новостей не найдено',
			'not_found_in_trash'	=> 'В корзине новостей не найдено',
			'parent_item_colon'		=> '',
			'menu_name'						=> 'Новости'

			),
		'public'							=> true,
		'publicly_queryable'	=> true,
		'show_ui'							=> true,
		'show_in_menu'				=> true,
		'query_var'						=> true,
		'rewrite'							=> true,
		'capability_type'			=> 'post',
		'has_archive'					=> true,
		'hierarchical'				=> false,
		'menu_position'				=> null,
		'supports'						=> array('title'),
		'taxonomies'	=> array('news_cat')
	) );
}

function news_cat() { 
	$labels = array( 
		'name'                       => ( 'Категории'),
		'singular_name'              => ( 'Категория'),
		'menu_name'                  => ( 'Категории'),
	); 
	$args = array( 
		'labels'                     => $labels, 
		'hierarchical'               => false, 
		'public'                     => true, 
		'show_ui'                    => true, 
		'show_admin_column'          => true, 
		'show_in_nav_menus'          => true, 
		'show_tagcloud'              => true, 
	); 
	register_taxonomy( 'news_cat', array( 'post_news' ), $args ); 
}
add_action( 'init', 'news_cat', 0 );

/* Новости */

/* Аналитика */

add_action('init', 'post_analitics');
function post_analitics(){
	register_post_type('post_analitics', array(
		'labels'							=> array(
			'name'								=> 'Аналитика', // Основное название типа записи
			'singular_name'				=> 'Аналитика', // отдельное название записи типа Book
			'add_new'							=> 'Добавить новую',
			'add_new_item'				=> 'Добавить новую аналитику',
			'edit_item'						=> 'Редактировать аналитику',
			'new_item'						=> 'Новая аналитика',
			'view_item'						=> 'Посмотреть аналитику',
			'search_items'				=> 'Найти аналитику',
			'not_found'						=> 'Аналитика не найдено',
			'not_found_in_trash'	=> 'В корзине аналитика не найдено',
			'parent_item_colon'		=> '',
			'menu_name'						=> 'Аналитика'

			),
		'public'							=> true,
		'publicly_queryable'	=> true,
		'show_ui'							=> true,
		'show_in_menu'				=> true,
		'query_var'						=> true,
		'rewrite'							=> true,
		'capability_type'			=> 'post',
		'has_archive'					=> true,
		'hierarchical'				=> false,
		'menu_position'				=> null,
		'supports'						=> array('title'),
		'taxonomies'	=> array('analitics_cat')
	) );
}

function analitics_cat() { 
	$labels = array( 
		'name'                       => ( 'Категории'),
		'singular_name'              => ( 'Категория'),
		'menu_name'                  => ( 'Категории'),
	); 
	$args = array( 
		'labels'                     => $labels, 
		'hierarchical'               => false, 
		'public'                     => true, 
		'show_ui'                    => true, 
		'show_admin_column'          => true, 
		'show_in_nav_menus'          => true, 
		'show_tagcloud'              => true, 
	); 
	register_taxonomy( 'analitics_cat', array( 'post_analitics' ), $args ); 
}
add_action( 'init', 'analitics_cat', 0 );

/* Аналитика */

/* Видео */

add_action('init', 'post_video');
function post_video(){
	register_post_type('post_video', array(
		'labels'							=> array(
			'name'								=> 'Видео', // Основное название типа записи
			'singular_name'				=> 'Видео', // отдельное название записи типа Book
			'add_new'							=> 'Добавить новое',
			'add_new_item'				=> 'Добавить новое видео',
			'edit_item'						=> 'Редактировать видео',
			'new_item'						=> 'Новое видео',
			'view_item'						=> 'Посмотреть видео',
			'search_items'				=> 'Найти видео',
			'not_found'						=> 'Видео не найдено',
			'not_found_in_trash'	=> 'В корзине видео не найдено',
			'parent_item_colon'		=> '',
			'menu_name'						=> 'Видео'

			),
		'public'							=> true,
		'publicly_queryable'	=> true,
		'show_ui'							=> true,
		'show_in_menu'				=> true,
		'query_var'						=> true,
		'rewrite'							=> true,
		'capability_type'			=> 'post',
		'has_archive'					=> true,
		'hierarchical'				=> false,
		'menu_position'				=> null,
		'supports'						=> array('title'),
		'taxonomies'	=> array('video_cat')
	) );
}

function video_cat() { 
	$labels = array( 
		'name'                       => ( 'Категории'),
		'singular_name'              => ( 'Категория'),
		'menu_name'                  => ( 'Категории'),
	); 
	$args = array( 
		'labels'                     => $labels, 
		'hierarchical'               => false, 
		'public'                     => true, 
		'show_ui'                    => true, 
		'show_admin_column'          => true, 
		'show_in_nav_menus'          => true, 
		'show_tagcloud'              => true, 
	); 
	register_taxonomy( 'video_cat', array( 'post_video' ), $args ); 
}
add_action( 'init', 'video_cat', 0 );

/* Видео */

/* СМИ о нас */

add_action('init', 'post_smi');
function post_smi(){
	register_post_type('post_smi', array(
		'labels'							=> array(
			'name'								=> 'СМИ о нас', // Основное название типа записи
			'singular_name'				=> 'СМИ о нас', // отдельное название записи типа Book
			'add_new'							=> 'Добавить новое',
			'add_new_item'				=> 'Добавить новую запись',
			'edit_item'						=> 'Редактировать запись',
			'new_item'						=> 'Новая запись',
			'view_item'						=> 'Посмотреть запись',
			'search_items'				=> 'Найти запись',
			'not_found'						=> 'Запись не найдена',
			'not_found_in_trash'	=> 'В корзине записей не найдено',
			'parent_item_colon'		=> '',
			'menu_name'						=> 'Сми о нас'

			),
		'public'							=> true,
		'publicly_queryable'	=> true,
		'show_ui'							=> true,
		'show_in_menu'				=> true,
		'query_var'						=> true,
		'rewrite'							=> true,
		'capability_type'			=> 'post',
		'has_archive'					=> true,
		'hierarchical'				=> false,
		'menu_position'				=> null,
		'supports'						=> array('title'),
		'taxonomies'	=> array('smi_cat')
	) );
}

function smi_cat() { 
	$labels = array( 
		'name'                       => ( 'Категории'),
		'singular_name'              => ( 'Категория'),
		'menu_name'                  => ( 'Категории'),
	); 
	$args = array( 
		'labels'                     => $labels, 
		'hierarchical'               => false, 
		'public'                     => true, 
		'show_ui'                    => true, 
		'show_admin_column'          => true, 
		'show_in_nav_menus'          => true, 
		'show_tagcloud'              => true, 
	); 
	register_taxonomy( 'smi_cat', array( 'post_smi' ), $args ); 
}
add_action( 'init', 'smi_cat', 0 );

/* СМИ о нас */

/* Хлебные крошки */


function get_breadcrumb() {
    echo '<a href="'.home_url().'" rel="nofollow">Главная</a>';
    if (is_category() || is_single()) {
        echo "&nbsp;&nbsp;&nbsp;&nbsp;&#8250;&nbsp;&nbsp;&nbsp;&nbsp;";
        echo ("<a href='/news/'>Новости</a>");
            if (is_single()) {
                echo "<div class='breadcrumb__fish'> &nbsp;&nbsp;&nbsp;&nbsp;&#8250;&nbsp;&nbsp;&nbsp;&nbsp; </div>";
                the_title();
            }
    } elseif (is_page()) {
        echo "<div class='breadcrumb__fish'> &nbsp;&nbsp;&nbsp;&nbsp;&#8250;&nbsp;&nbsp;&nbsp;&nbsp; </div>";
        echo the_title();
    } elseif (is_search()) {
        echo "<div class='breadcrumb__fish'> &nbsp;&nbsp;&nbsp;&nbsp;&#8250;&nbsp;&nbsp;&nbsp;&nbsp;Search Results for... </div>";
        echo '"<em>';
        echo the_search_query();
        echo '</em>"';
    }
}

/* Хлебные крошки */



function wp_theme_styles() {
	wp_enqueue_style( 'wp-bootstrap-starter-bootstrap-css', get_template_directory_uri() . '/inc/assets/css/bootstrap.min.css' );
	wp_enqueue_style( 'fancybox-css', get_stylesheet_directory_uri().'/css/jquery.fancybox.min.css' );
	wp_enqueue_style( 'animate-css', get_stylesheet_directory_uri().'/css/animate.css' );
	wp_enqueue_style( 'slick-css', get_stylesheet_directory_uri().'/css/slick.css' );
	wp_enqueue_style( 'style-css', get_stylesheet_directory_uri().'/css/style.css' );
}
add_action( 'wp_enqueue_scripts', 'wp_theme_styles' );

function wp_theme_scripts() {
	wp_enqueue_script('jquery');
	wp_enqueue_script('fancybox-js', get_stylesheet_directory_uri() . '/js/jquery.fancybox.min.js');
	wp_enqueue_script('main-js', get_stylesheet_directory_uri() . '/js/main.js');
	wp_enqueue_script('cf7-addon-js', get_stylesheet_directory_uri() . '/js/cf7-addon.js');
	wp_enqueue_script('mask-input-js', get_stylesheet_directory_uri() . '/js/jquery.maskedinput.js');
	wp_enqueue_script('slick-js', get_stylesheet_directory_uri() . '/js/slick.js');
}
add_action( 'wp_enqueue_scripts', 'wp_theme_scripts' );

add_filter('wp_image_editors', function(){
	return array('WP_Image_Editor_GD');
});
