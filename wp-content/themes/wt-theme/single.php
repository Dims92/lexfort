<?php
/**
 * Template for displaying single post (read full post page).
 * 
 * @package bootstrap-basic
 */

get_header();
?>

<div class="page-single w100">
	<?php
		while ( have_posts() ) : the_post();
			get_template_part( 'template-parts/content-page', get_post_format() );
		endwhile; // End of the loop.
	?>
</div>

<?php get_footer(); ?>
