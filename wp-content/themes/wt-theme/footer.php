 <?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Bootstrap_Starter
 */

?>
<?php if(!is_page_template( 'blank-page.php' ) && !is_page_template( 'blank-page-with-container.php' )): ?>
</div>
	<?php get_template_part( 'footer-widget' ); ?>
	<?php $footer_bg = get_field('footer-bg', 'option'); ?>
	<footer class="footer" role="contentinfo" style="background-image: url(<?php echo $footer_bg['url']; ?>);">
		<div class="footer-container container flex">
			<div class="logo-column">
				<?php $logo = get_field('logo-footer', 'option'); ?>
				<a href="/" class="footer-logo-link flex flex-jcc flex-alc">
					<img src="<?php echo $logo['url']; ?>" alt="<?php echo $logo['title']; ?>">
				</a>
				<div class="footer-social">
					<? if(have_rows('social-list', 'option')): ?>
						<ul class="header-top-row__social-list flex">
							<?php while (have_rows('social-list', 'option')): the_row();
								$social_link = get_sub_field('social-link', 'option');
								$social_icon = get_sub_field('social-icon', 'option');
							?>
								<li class="header-top-row__social-item flex">
									<a href="<?php echo $social_link; ?>" class="header-top-row__social-link flex flex-jcc flex-alc" target="_blank">
										<img src="<?php echo $social_icon['sizes']['about_us_img']; ?>" alt="<?php echo $social_icon['title']; ?>">
									</a>
								</li>
							<?php endwhile; ?>
						</ul>
					<?php endif; ?>
				</div>
			</div>

			<div class="footer-menu-block flex flex-dc">
				<p class="footer-block-title"> Меню
				<nav class="footer-menu-wrap flex">
					<?php
						wp_nav_menu(array(
							'menu'            => 'footer-left',
							'container'       => false,
							'menu_id'         => 'footer-left',
							'menu_class'      => 'footer-menu footer-menu--left flex flex-dc',
							'depth'           => 3,
						));
					?>
					<?php
						wp_nav_menu(array(
							'menu'            => 'footer-right',
							'container'       => false,
							'menu_id'         => 'footer-right',
							'menu_class'      => 'footer-menu footer-menu--right flex flex-dc',
							'depth'           => 3,
						));
					?>
				</nav>
			</div>

			<div class="footer-contacts footer-contacts--moscow flex flex-dc">
				<p class="footer-block-title">Москва</p>
				<a href="<?php the_field('adress-link-moscow', 'option') ?>" class="footer-contacts-link">
					<?php the_field('adress-text-moscow', 'option') ?>
				</a>
				<? if(have_rows('phone-num-moscow', 'option')): ?>
					<ul class="footer-contacts__list flex flex-dc">
						<?php while (have_rows('phone-num-moscow', 'option')): the_row();
							$address_link = get_sub_field('phone-link', 'option');
							$address_text = get_sub_field('phone-text', 'option');
						?>
							<li class="footer-contacts__item flex">
								<a href="<?php echo $address_link; ?>" class="footer-contacts__link flex" target="_blank">
									<?php echo $address_text; ?>
								</a>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
				<a href="<?php the_field('e-mail-moscow', 'option') ?>" class="footer-contacts-link">
					<?php the_field('e-mail-moscow', 'option') ?>
				</a>
			</div>

			<div class="footer-contacts footer-contacts--omsk flex flex-dc">
				<p class="footer-block-title">Омск</p>
				<a href="<?php the_field('adress-link-omsk', 'option') ?>" class="footer-contacts-link">
					<?php the_field('adress-text-omsk', 'option') ?>
				</a>
				<? if(have_rows('phone-num-omsk', 'option')): ?>
					<ul class="footer-contacts__list flex flex-dc">
						<?php while (have_rows('phone-num-omsk', 'option')): the_row();
							$address_link = get_sub_field('phone-link', 'option');
							$address_text = get_sub_field('phone-text', 'option');
						?>
							<li class="footer-contacts__item flex">
								<a href="<?php echo $address_link; ?>" class="footer-contacts__link flex" target="_blank">
									<?php echo $address_text; ?>
								</a>
							</li>
						<?php endwhile; ?>
					</ul>
				<?php endif; ?>
				<a href="<?php the_field('e-mail-omsk', 'option') ?>" class="footer-contacts-link">
					<?php the_field('e-mail-omsk', 'option') ?>
				</a>
			</div>

			<div class="footer-policy-block flex flex-dc">
				<a href="#" class="header-bottom-row__log-in header-bottom-row__log-in--footer all-button-body flex">
					<p>Личный кабинет</p>
				</a>
				
				<?php $policy_id = get_field('policy', 'option'); ?>
				<a href="<?php echo get_the_permalink($policy_id); ?>"  class="footer-bottom__link-policy"><?php echo get_the_title($policy_id); ?></a>

				<address class="developed-block">
					<p>Сайт разработан </p><a href="https://asmart-group.ru/", target="blank">IT-company ASMART</a>
				</address>

			</div>
		</div>
	</footer><!-- #colophon -->
<?php endif; ?>

	</div><!-- #content -->
</div><!-- #page -->

<div style="display:none" class="fancybox-hidden">
	<div id="modal-form" class="site-modal form">
		<?php echo do_shortcode('[contact-form-7 id="9" title="Форма заявки"]'); ?>
	</div>
</div>

<?php wp_footer(); ?>

<!-- Modals -->

<!-- окно обработки форм (корректная отправка) -->
<div class="modal fade" id="cf-success-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"></span></button>
			</div>
			<div class="modal-body flex">
				<div class="title cmodal-title text-center text-red">Message send</div>
				<div class="message text-center"></div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div>

<!-- окно обработки форм (ошибки заполнения) -->
<!-- <div class="modal fade" id="cf-modal" tabindex="-1" role="dialog">
	<div class="modal-dialog modal-md" role="document">
		<div class="modal-content">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times" aria-hidden="true"></i></span></button>
			<div class="modal-header">
			</div>
			<div class="modal-body">
				<div class="title cmodal-title text-center text-red">Ошибка!</div>
				<div class="message text-center mt-3"></div>
			</div>
			<div class="modal-footer">
			</div>
		</div>
	</div>
</div> -->

<!-- END Modals-->

</body>
</html>
