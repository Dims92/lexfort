jQuery(document).ready(function() {
	jQuery('.slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		arrows: true,
		dots: false,
		prevArrow: jQuery('.slider-arrow-prev'),
		nextArrow: jQuery('.slider-arrow-next')
	});
	jQuery('.mobile-menu').click(function(){
		jQuery(this).toggleClass('active');
		jQuery('.header-menu').toggleClass('active');
		jQuery('body').toggleClass('overflow-hidden');
		jQuery('header').toggleClass('menu-open');
	});
	jQuery('.header-menu li:not(.menu-item-has-children) > a').click(function(){
		jQuery('.mobile-menu, .header-menu').removeClass('active');
		jQuery('body').removeClass('overflow-hidden');
		jQuery('header').removeClass('menu-open');
	});
	jQuery(document.body).on('click', function(e){
		if(!jQuery(e.target).closest('.mobile-menu').length && !jQuery(e.target).closest('.header-menu').length){
			jQuery('.mobile-menu, .header-menu').removeClass('active');
			jQuery('body').removeClass('overflow-hidden');
			jQuery('header').removeClass('menu-open');
		};
	});
	jQuery('.header-menu li:not(.menu-item-has-children) > a[href*="/#"], .header-menu li:not(.menu-item-has-children) > a[href*="#"], .footer-menu a[href*="/#"], .footer-menu a[href*="#"]').click(function(event) {
		if (
				location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') 
				&& location.hostname == this.hostname
			 ) {
			var target = jQuery(this.hash);
			target = target.length ? target : jQuery('[name=' + this.hash.slice(1) + ']');
			if (target.length) {
				event.preventDefault();
				jQuery('html, body').animate({
					scrollTop: target.offset().top - 100
				}, 1000);
			}
		};
	});
	jQuery(window).scroll(function(){
		var scroll = jQuery(this).scrollTop();
		if (scroll > 150) {
			jQuery('header').addClass('header-active');
		} else {
			jQuery('header').removeClass('header-active');
		};
	});
	document.addEventListener( 'wpcf7mailsent', function( event ) {
		jQuery.fancybox.close();
	}, false );
	jQuery('input[type="tel"]').mask('+7(999) 999-99-99');

	/* Слайдер команды на главной */

	if(jQuery('.js-main-employees__list').length > 0) {
		jQuery('.js-main-employees__list').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			infinite: true,
			arrows: true,
			dots: false,
			fade: true,
			adaptiveHeight: true,
			cssEase: 'linear',
			asNavFor: '.js-main-employees__list-photo, .js-main-employees__list-photo-2',
			prevArrow: jQuery('.js-employees-slider-arrow-prev'),
			nextArrow: jQuery('.js-employees-slider-arrow-next'),
		});
		var bannerSlider = jQuery('.js-main-employees__list');
		jQuery('.banner-sl-count__total').text( bannerSlider.slick("getSlick").slideCount);
		jQuery(".js-main-employees__list").on('afterChange', function(event, slick, currentSlide){
			jQuery(".banner-sl-count__current").text(currentSlide + 1);
		});

		jQuery('.js-main-employees__list-photo').slick({
			slidesToShow: 2,
			slidesToScroll: 1,
			infinite: true,
			arrows: false,
			dots: false,
			asNavFor: '.js-main-employees__list, .js-main-employees__list-photo-2',
			responsive: [
				{
				  breakpoint: 1001,
				  settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					infinite: true,
					arrows: false,
					dots: false,
					fade: true,
					cssEase: 'linear',
					asNavFor: '.js-main-employees__list, .js-main-employees__list-photo-2'
				  }
				}
			]
		});
	}

	/* открытие бургера меню при адаптиве */

	if (jQuery('.js-header-desctop-main__burger-icon, .js-header-desctop-main__burger-content').length > 0) {
		jQuery('.js-header-desctop-main__burger-icon').on('click', function () {
			event.preventDefault();
			jQuery('.js-header-desctop-main__burger-content').fadeToggle();
			jQuery('.js-header-desctop-main__burger-icon').toggleClass('menu-open');
		});
	}

	/* Плавная прокрутка до якоря */

	if (jQuery('[href*="#"]').length > 0) {
		jQuery("body").on('click', '[href*="#"]', function(e){
			var fixed_offset = 50;
			jQuery('html,body').stop().animate({ scrollTop: jQuery(this.hash).offset().top - fixed_offset }, 1000);
			e.preventDefault();
		});
	}


});

jQuery(document).ajaxComplete(function(data) {
	jQuery('.wpcf7-tel').mask('+7(999) 999-9999');
});
